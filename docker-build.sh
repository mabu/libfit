#/bin/sh

## Simple docker script to test CI

## Get the wanted image
docker pull gcc

## Build all the stuff
docker run \
    --rm=true\
    --mount type=bind,source=$PWD,target=/libfit\
    gcc\
    /bin/bash -c "apt update; apt upgrade -y; apt install -y libjson-c-dev libxml2-dev; cd libfit; make clean; make"

