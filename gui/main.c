#include <gtk/gtk.h>

#include "functions.h"
#include "gui.h"
#include "util.h"
#include "types.h"

int main (int argc, char *argv[])
{
    struct ui *ui;
    gtk_init(&argc, &argv);

    /* load interface */
    ui = build_interface();
    
    /* load files list */
    struct node * files = NULL;
    files = find_files(NULL, argv[1], ".FIT"); 
    struct node *next = files;
    while (next)
    {
        //GtkWidget *label = gtk_label_new(next->name);
        //gtk_list_box_insert(GTK_LIST_BOX(ui->listBox), label, -1);
        add_to_tree(ui->treeView, next->name);

        next = next->next;
    }

    /* select first file */

    /* prepare ui */
    gtk_widget_show_all(ui->window);

    /* start */
    gtk_main();
    
    return 0;
}

