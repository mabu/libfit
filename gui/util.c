#include <stdio.h>

#include "util.h"

void *xcalloc(size_t num, size_t size)
{
    void *r = calloc(num, size);
    if (!r)
        die("calloc");
    return r;
}

