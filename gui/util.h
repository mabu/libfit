#include <stdlib.h>

#define die(reason) do {perror(reason); exit(EXIT_FAILURE);} while(0)

void *xcalloc(size_t num, size_t size);
    
