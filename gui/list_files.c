#include <string.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <linux/limits.h>

#include "types.h"

struct node * insert_node(struct node *tree, struct node *node)
{
    if (!tree)
        return node;

    while(tree->next)
        tree = tree->next;
    
    tree->next = node;
    return node;
}

void free_tree(struct node *tree)
{
    struct node *next = NULL;

    if (tree && tree->next)
        next = tree->next;

    free(tree->path);
    free(tree);
    free_tree(next);

}

struct node * find_files(struct node *ret, const char *root, const char *wext)
{
    GFile *path;
    GFileInfo *fi;
    GFileEnumerator *fe;
    struct node *node = NULL, *last = NULL;

    path = g_file_new_for_path(root);

    fe = g_file_enumerate_children(path, "*", 
         G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS,
         NULL, NULL);
    
    while (1)
    {
        fi = g_file_enumerator_next_file(fe, NULL, NULL); 
        if (!fi)
            break;
        
        if (G_FILE_TYPE_DIRECTORY == g_file_info_get_file_type(fi))
        {
            char * mem = malloc (2 + strlen(root) + strlen(g_file_info_get_name(fi)));
            if (mem)
            {
                sprintf(mem, "%s/%s", root, g_file_info_get_name(fi));
                ret = find_files(ret, mem, wext);         
                free(mem);
            }
        }
        else if (G_FILE_TYPE_REGULAR == g_file_info_get_file_type(fi))
        {
            const char *name = g_file_info_get_name(fi);
            const char *ext = strrchr(name, '.');
            char rel_path [PATH_MAX + 1];
            char path [PATH_MAX + 1];
            if (ext && (0 == strcmp(ext, wext)))
            {
                sprintf(rel_path, "%s/%s", root, name);
                realpath(rel_path, path);
                //g_print("%s\n", path);
                node = calloc(1, sizeof *node);
                
                if (!ret)
                {
                    ret = last = node;
                    //g_print("ret: %p\n", ret);
                }
                else
                {
                    last = insert_node(last?last:ret, node);
                }

                last->path = strdup(path);
                last->name = strrchr(last->path, '/');
                if (!last->name)
                    last->name = last->path;
                else
                    last->name++;
            }
        }
        g_object_unref(fi);
    }

    g_object_unref(fe);
    g_object_unref(path);

    return ret;
}


