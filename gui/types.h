#ifndef H_20190627_TYPES
#define H_20190627_TYPES

struct node 
{
    struct node * next;
    char * path;
    char * name;
};

struct ui
{
    GtkWidget *window;
    GtkWidget *treeView;
    GtkWidget *image;
    GtkWidget *textArea;
};



#endif
