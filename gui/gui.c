#include <gtk/gtk.h>
#include "util.h"
#include "types.h"
#include "gui.h"

enum COLUMNS {
    NAME,
    // PATH,
    NB_COL
};


void on_destroy(GtkWidget *widget, gpointer user_data)
{
    exit(0);
}

void init_list(GtkWidget *treeView)
{
    GtkCellRenderer *renderer;
    GtkTreeViewColumn *column;
    GtkListStore *store;

    renderer = gtk_cell_renderer_text_new ();
    column = gtk_tree_view_column_new_with_attributes("List Items",
        renderer, "text", NAME, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeView), column);

    store = gtk_list_store_new(NB_COL, G_TYPE_STRING);

    gtk_tree_view_set_model(GTK_TREE_VIEW(treeView), 
            GTK_TREE_MODEL(store));

    g_object_unref(store);
}

void add_to_tree(GtkWidget *treeView, const gchar *str) 
{
    GtkListStore *store;
    GtkTreeIter iter;

    store = GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(treeView)));

    gtk_list_store_append(store, &iter);
    gtk_list_store_set(store, &iter, NAME, str, -1);
}


void row_activated(GtkTreeView *tree, GtkTreePath *path,
        GtkTreeViewColumn *col, gpointer user_data)

{
    /* get the file selected path */
    GtkTreeIter iter;
    gchar *value;

    GtkTreeModel *model = gtk_tree_view_get_model(tree);

    if (gtk_tree_model_get_iter(model, &iter, path)) 
    {
        //gtk_tree_model_get (GTK_LIST_STORE(model), &iter, 0, &int_data, 1, &str_data, -1));
        gtk_tree_model_get(model, &iter, NAME, &value,  -1);
        g_print("%s selected!\n", value);
        g_free(value);

    
    }
    /* check if it's in data base */
    /* if not
     *    compute stat
     *    compute trace */
    /* display stats */
    /* display trace */

}


/*
 * Function call when user click on a file name
 */
void row_selected(GtkListBox * box, GtkListBoxRow * row,
                  gpointer user_data)
{
    /* update global selection */
    g_print("%s(%p, %p, %p)!\n", __FUNCTION__, box, row, user_data);
}

struct ui * build_interface(void)
{
    GtkBuilder *builder;
    struct ui* r = xcalloc(1, sizeof *r);
    builder = gtk_builder_new_from_file("gui.xml");
    
    // main window    
    r->window = GTK_WIDGET(gtk_builder_get_object(builder, "MainWindow"));
    gtk_builder_connect_signals(builder, NULL);


    // list box
    r->treeView = GTK_WIDGET(gtk_builder_get_object(builder, "FileList"));
   
    init_list(r->treeView);


/*    gtk_list_box_set_selection_mode(GTK_LIST_BOX(r->listBox), 
            GTK_SELECTION_MULTIPLE); 

    gtk_list_box_set_activate_on_single_click(GTK_LIST_BOX(r->listBox), FALSE);
*/
    
    // image view
    r->image = GTK_WIDGET(gtk_builder_get_object(builder, "ImageView"));

    // text area
    r->textArea = GTK_WIDGET(gtk_builder_get_object(builder, "TextView"));

    g_object_unref(builder);
    
    return r;
}


