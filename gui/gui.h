#ifndef H_20190421_GUI
#define H_20190421_GUI
#include <gtk/gtk.h>

struct ui * build_interface(void);
void add_to_tree(GtkWidget *treeView, const gchar *str);
//void row_activated(GtkListBox *box, GtkListBoxRow *row, gpointer user_data);
void row_activated(GtkTreeView*, GtkTreePath*, GtkTreeViewColumn*, gpointer);
void row_selected(GtkListBox *box, GtkListBoxRow *row, gpointer user_data);
void on_destroy(GtkWidget *widget, gpointer user_data);
#endif
