#ifndef H_20180211_LIBFIT_MESSAGES
#define H_20180211_LIBFIT_MESSAGES

#define FIT_TYPE_enum       0
#define FIT_TYPE_int8       1
#define FIT_TYPE_uint8      2
#define FIT_TYPE_int16      0x83
#define FIT_TYPE_uint16     0x84
#define FIT_TYPE_int32      0x85
#define FIT_TYPE_uint32     0x86
#define FIT_TYPE_string     7
#define FIT_TYPE_float      0x88
#define FIT_TYPE_double     0x89
#define FIT_TYPE_uint8z     0x0A
#define FIT_TYPE_uint16z    0x8B
#define FIT_TYPE_uint32z    0x8C
#define FIT_TYPE_bytes      0x0D
#define FIT_TYPE_int64      0x8E
#define FIT_TYPE_uint64     0x8F
#define FIT_TYPE_uint64z    0x90


#define FIT_GLOBAL_FILE_ID          0
#define FIT_GLOBAL_CAPABILITIES     1
#define FIT_GLOBAL_DEVICE_SETTINGS  2
#define FIT_GLOBAL_USER_PROFILE     3
#define FIT_GLOBAL_HRM_PROFILE      4
#define FIT_GLOBAL_SDM_PROFILE      5
#define FIT_GLOBAL_BIKE_PROFILE     6
#define FIT_GLOBAL_ZONES_TARGET     7
#define FIT_GLOBAL_HR_ZONE          8
#define FIT_GLOBAL_POWER_ZONE       9
#define FIT_GLOBAL_MET_ZONE         10
#define FIT_GLOBAL_SPORT            12
#define FIT_GLOBAL_GOAL             15
#define FIT_GLOBAL_SESSION          18
#define FIT_GLOBAL_LAP              19
#define FIT_GLOBAL_RECORD           20
#define FIT_GLOBAL_EVENT            21
#define FIT_GLOBAL_DEVICE_INFO      23
#define FIT_GLOBAL_WORKOUT          26
#define FIT_GLOBAL_WORKOUT_STEP     27
#define FIT_GLOBAL_SCHEDULE         28
#define FIT_GLOBAL_WEIGHT_SCALE     30
#define FIT_GLOBAL_COURSE           31
#define FIT_GLOBAL_COURSE_POINT     32
#define FIT_GLOBAL_TOTALS           33
#define FIT_GLOBAL_ACTIVITY         34
#define FIT_GLOBAL_SOFTWARE         35
#define FIT_GLOBAL_FILE_CAP         37
#define FIT_GLOBAL_MESG_CAP         38
#define FIT_GLOBAL_FIELD_CAP        39
#define FIT_GLOBAL_FILE_CREATOR     49
#define FIT_GLOBAL_BLOOD_PRESSURE   51
#define FIT_GLOBAL_SPEED_ZONE       53
#define FIT_GLOBAL_MONITORING       55
#define FIT_GLOBAL_TRAINING_FILE    72
#define FIT_GLOBAL_HRV              78
/* see https://github.com/scrapper/fit4ruby/blob/master/lib/fit4ruby/GlobalFitMessages.rb */
#define FIT_GLOBAL_USER_DATA        79
#define FIT_GLOBAL_LENGTH           101
#define FIT_GLOBAL_MONITORING_INFO  103
#define FIT_GLOBAL_PAD              105
#define FIT_GLOBAL_SLAVE_DEVICE     106
#define FIT_GLOBAL_CADENCE_ZONE     131
#define FIT_GLOBAL_NOT_DOC_140      140
#define FIT_GLOBAL_SEGMENT_LAP      142
#define FIT_GLOBAL_MEMO_GLOB        145
#define FIT_GLOBAL_SEGMENT_ID       148
#define FIT_GLOBAL_SEGMENT_LB_ENT   149
#define FIT_GLOBAL_SEGMENT_POINT    150
#define FIT_GLOBAL_SEGMENT_FILE     151
#define FIT_GLOBAL_GPS_METADATA     160
#define FIT_GLOBAL_CAMERA_EVENT     161
#define FIT_GLOBAL_TIMESTAMP_CORRELATION        162
#define FIT_GLOBAL_GYROSCOPE_DATA               164
#define FIT_GLOBAL_ACCELEROMETER_DATA           165
#define FIT_GLOBAL_THREE_D_SENSOR_CALIBRATION   167
#define FIT_GLOBAL_VIDEO_FRAME                  169
#define FIT_GLOBAL_OBDII_DATA                   174
#define FIT_GLOBAL_NMEA_SENTENCE                177
#define FIT_GLOBAL_AVIATION_ATTITUDE            178
#define FIT_GLOBAL_VIDEO                        184
#define FIT_GLOBAL_VIDEO_TITLE                  185
#define FIT_GLOBAL_VIDEO_DESCRIPTION            186
#define FIT_GLOBAL_VIDEO_CLIP                   187
#define FIT_GLOBAL_MFG_RANGE_MIN                0XFF00
#define FIT_GLOBAL_MFG_RANGE_MAX                0XFFFE


/* fields for FIT_GLOBAL_FILE_CREATOR */
#define FIT_FILE_CREATOR_SOFTWARE_VERSION_U16 0
#define FIT_FILE_CREATOR_HARDWARE_VERSION_U8  1

/* fields for FIT_GLOBAL_FILE_ID */
#define FIT_FILE_TYPE           0
#define FIT_FILE_MANUFACTURER   1
#define FIT_FILE_PRODUCT        2
#define FIT_FILE_SERIAL_NUMBER  3
#define FIT_FILE_TIME_STAMP     4
#define FIT_FILE_NUMBER         5

/* fields for FIT_GLOBAL_RECORD */
#define FIT_RECORD_POSITION_LAT_S32     0
#define FIT_RECORD_POSITION_LONG_S32    1
#define FIT_RECORD_ALTITUDE_U16         2
#define FIT_RECORD_HEART_RATE_U8        3
#define FIT_RECORD_DISTANCE_U32         5
#define FIT_RECORD_SPEED_U16            6
#define FIT_RECORD_CALORIES             33
#define FIT_RECORD_ZONE                 50
#define FIT_RECORD_FRACTIONAL_CADENCE   53
#define FIT_RECORD_TIMESTAMP_U32        253

/* fields for FIT_GLOBAL_SESSION */
#define FIT_SESSION_MESSAGE_INDEX_U16       254
#define FIT_SESSION_TIMESTAMP_U32           253
#define FIT_SESSION_EVENT_ENUM              0
#define FIT_SESSION_EVENT_TYPE_ENUM         1
#define FIT_SESSION_START_TIME_U32          2
#define FIT_SESSION_START_POSITION_LAT_S32  3
#define FIT_SESSION_START_POSITION_LONG_S32 4
#define FIT_SESSION_SPORT_ENUM              5
#define FIT_SESSION_SPORT_ENUM_GENERIC      0
#define FIT_SESSION_SPORT_ENUM_RUNNING      1
#define FIT_SESSION_SPORT_ENUM_CYCLING      2
#define FIT_SESSION_SPORT_ENUM_WALKING      11
#define FIT_SESSION_SUB_SPORT_ENUM          6
#define FIT_SESSION_TOTAL_ELAPSED_TIME_U32  7
#define FIT_SESSION_TOTAL_TIMER_TIME_U32    8
#define FIT_SESSION_TOTAL_DISTANCE_U32      9
#define FIT_SESSION_TOTAL_CALORIES_U16      11
#define FIT_SESSION_AVG_SPEED_U16           14
#define FIT_SESSION_AVG_HEART_RATE          16
#define FIT_SESSION_MAX_HEART_RATE          17
#define FIT_SESSION_FIRST_LAP_INDEX_U16     25
#define FIT_SESSION_NUM_LAPS_U16            26
#define FIT_SESSION_TRIGGER_ENUM            28
#define FIT_SESSION_MIN_HEART_RATE          64

/* fileds for FIT_GLOBAL_LAP (19) */
#define FIT_LAP_TIMESTAMP                   253
#define FIT_LAP_EVENT                       0
#define FIT_LAP_EVENT_TYPE                  1
#define FIT_LAP_START_TIME                  2
#define FIT_LAP_START_POSITION_LAT          3
#define FIT_LAP_START_POSITION_LON          4
#define FIT_LAP_END_POSITION_LAT            5
#define FIT_LAP_END_POSITION_LON            6
#define FIT_LAP_ELAPSED_TIME                7
#define FIT_LAP_TIMER_TIME                  8
#define FIT_LAP_DISTANCE                    9
#define FIT_LAP_CYCLES                      10
#define FIT_LAP_CALORIES                    11
#define FIT_LAP_FAT_CALORIES                12
#define FIT_LAP_AVG_SPEED                   13
#define FIT_LAP_MAX_SPEED                   14
#define FIT_LAP_AVG_HR                      15
#define FIT_LAP_MAX_HR                      16



/* fields for FIT_GLOBAL_COURSE_POINT (32) */
#define FIT_COURSE_POINT_GENERIC 0
#define FIT_COURSE_POINT_TIMESTAMP 1
#define FIT_COURSE_POINT_POSITION_LAT 2
#define FIT_COURSE_POINT_POSITION_LONG 3
#define FIT_COURSE_POINT_DISTANCE 4
#define FIT_COURSE_POINT_NAME 6
#define FIT_COURSE_POINT_MESSAGE_INDEX 254
#define FIT_COURSE_POINT_TYPE 5
#define FIT_COURSE_POINT_FAVORITE 8

/* fields for FIT_GLOBAL_EVENT (21) */
#define FIT_EVENT_TIMESTAMP_U32     253
#define FIT_EVENT_TIMER             0
#define FIT_EVENT_TYPE              1
#define FIT_EVENT_DATA_16           2
#define FIT_EVENT_WORKOUT           3
#define FIT_EVENT_WORKOUT_STEP      4
#define FIT_EVENT_HR_HIGH_ALERT     13
#define FIT_EVENT_HR_LOW_ALERT      14
#define FIT_EVENT_SPEED_HIGH_ALERT  15
#define FIT_EVENT_POWER_HIGH_ALERT  19
#define FIT_EVENT_POWER_LOW_ALERT   20

/* fields for FIT_GLOBAL_DEVICE_INFO (23) */

#define FIT_DEVICE_INFO_DEVICE_INDEX          0
#define FIT_DEVICE_INFO_DEVICE_TYPE           1
#define FIT_DEVICE_INFO_MANUFACTURER          2
#define FIT_DEVICE_INFO_SERIAL_NUMBER         3
#define FIT_DEVICE_INFO_PRODUCT               4
#define FIT_DEVICE_INFO_SOFTWARE_VERSION      5
#define FIT_DEVICE_INFO_HARDWARE_VERSION      6
#define FIT_DEVICE_INFO_CUM_OPERATING_TIME    7
#define FIT_DEVICE_INFO_BATTERY_VOLTAGE       10
#define FIT_DEVICE_INFO_BATTERY_STATUS        11
#define FIT_DEVICE_INFO_SENSOR_POSITION       18
#define FIT_DEVICE_INFO_DESCRIPTOR            19
#define FIT_DEVICE_INFO_ANT_TRANSMISSION_TYPE 20
#define FIT_DEVICE_INFO_ANT_DEVICE_NUMBER     21
#define FIT_DEVICE_INFO_ANT_NETWORK           22
#define FIT_DEVICE_INFO_SOURCE_TYPE           25
#define FIT_DEVICE_INFO_PRODUCT_NAME          27
#define FIT_DEVICE_INFO_TIMESTAMP             253


/* fields for FIT_GLOBAL_USER_DATA (79) */
//#define FIT_USER_DATA_VO2_MAX   0
#define FIT_USER_DATA_AGE       1
#define FIT_USER_DATA_HEIGHT    2
#define FIT_USER_DATA_WEIGHT    3
#define FIT_USER_DATA_GENDER    4

/* USER_PROFILE */
#define FIT_USER_PROFILE_FRIENDLY_NAME 0
#define FIT_USER_PROFILE_MESSAGE_INDEX 254
#define FIT_USER_PROFILE_WEIGHT 4
#define FIT_USER_PROFILE_LOCAL_ID 22
#define FIT_USER_PROFILE_USER_RUNNING_STEP_LENGTH 31
#define FIT_USER_PROFILE_USER_WALKING_STEP_LENGTH 32
#define FIT_USER_PROFILE_GENDER 1
#define FIT_USER_PROFILE_AGE 2
#define FIT_USER_PROFILE_HEIGHT 3
#define FIT_USER_PROFILE_LANGUAGE 5
#define FIT_USER_PROFILE_ELEV_SETTING 6
#define FIT_USER_PROFILE_WEIGHT_SETTING 7
#define FIT_USER_PROFILE_RESTING_HEART_RATE 8
#define FIT_USER_PROFILE_DEFAULT_MAX_RUNNING_HEART_RATE 9
#define FIT_USER_PROFILE_DEFAULT_MAX_BIKING_HEART_RATE 10
#define FIT_USER_PROFILE_DEFAULT_MAX_HEART_RATE 11
#define FIT_USER_PROFILE_HR_SETTING 12
#define FIT_USER_PROFILE_SPEED_SETTING 13
#define FIT_USER_PROFILE_DIST_SETTING 14
#define FIT_USER_PROFILE_POWER_SETTING 16
#define FIT_USER_PROFILE_ACTIVITY_CLASS 17
#define FIT_USER_PROFILE_POSITION_SETTING 18
#define FIT_USER_PROFILE_TEMPERATURE_SETTING 21
#define FIT_USER_PROFILE_GLOBAL_ID 23
#define FIT_USER_PROFILE_HEIGHT_SETTING 30

/* FIT_GLOBAL_NOT_DOC_140 */
#define FIT_NOT_DOC_VO2MAX  7


/*FIT_GLOBAL_SPORT */

/* FIT_GLOBAL_MONITORING */
#define FIT_MONITORING_TIMESTAMP        253
#define FIT_MONITORING_DEVICE_INDEX     0
#define FIT_MONITORING_CALORIES         1
#define FIT_MONITORING_DISTANCE         2
#define FIT_MONITORING_CYCLES           3
#define FIT_MONITORING_ACTIVE_TIME      4
#define FIT_MONITORING_ACTIVITY_TYPE    5
#define FIT_MONITORING_ACTIVITY_SUBTYPE 6
#define FIT_MONITORING_DISTANCE_16      8
#define FIT_MONITORING_CYCLES_16        9
#define FIT_MONITORING_ACTIVE_TIME_16   10
#define FIT_MONITORING_LOCAL_TIMESTAMP  11
#define FIT_MONITORING_DURATION_MIN     29
#define FIT_MONITORING_DURATION_SEC     30




#endif
