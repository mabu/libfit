#ifndef H_20171008_LIBFIT_INTERNAL
#define H_20171008_LIBFIT_INTERNAL

#include "libfit.h"

#ifdef NDEBUG
#define LOG_DEBUG(...) do {} while (0)
#else
#define LOG_DEBUG(...) do \
{\
    fprintf(stderr, "%s(%d): ", __FILE__, __LINE__);\
    fprintf(stderr, __VA_ARGS__);\
    fprintf(stderr, "\n");\
    fflush(stderr);\
} while (0)
#endif


#define LOG(...) do \
{\
    fprintf(stderr, "%s(%d): ", __FILE__, __LINE__);\
    fprintf(stderr, __VA_ARGS__);\
    fprintf(stderr, "\n");\
    fflush(stderr);\
} while (0)

typedef struct fit_header fit_header_t;
typedef struct fit_fixed_header fit_fixed_header_t;
typedef struct fit_field_def fit_field_def_t;
typedef struct fit_def fit_def_t;

#define TYPE_DATA 0
#define TYPE_DEFINITION 1

#pragma pack(push, 1)
struct fit_normal_header
{
    uint8_t local_message_type:4;
    /* 0: record 1: definition */
    uint8_t reserved:1;
    uint8_t developer_flag:1;
    uint8_t message_type:1;
    uint8_t normal_header:1;
};

struct fit_header
{
    uint8_t size;
    uint8_t proto_version;
    uint16_t profile_version;
    uint32_t data_size;
    char type[4];
    uint16_t CRC; 
};

struct fit_fixed_header
{
    uint8_t reserved;
    uint8_t arch;
    uint16_t message_number;
    uint8_t fields_cnt;
};

struct fit_field_def
{
    uint8_t field_definition_num;
    uint8_t size;
    uint8_t base_type;
};

struct fit_def
{
    // replace with index
    uint8_t index;
    uint8_t size;
    uint8_t fields_count;
    uint8_t arch;
    fit_field_def_t *fields;
};

struct fit_record {
    fit_def_t *definition;
    char *data;
};

#pragma pack(pop)
    

/* internal functions */
//const char * type_name(int type);


#endif

