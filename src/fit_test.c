/**
 fit_test is a that will read latitude / longitude for all record
 of a given FIT file.
 coodinates are computed twice: once directly, and a second time using
 FIT_getRatio

 */
#include <stdio.h>
#include "libfit.h"
#include "libfit-messages.h"

int main(int argc, char *argv[])
{
    puts(FIT_info());
    fit_record_t *rec = NULL, *recycle = NULL;
    while (--argc)
    {
        int cnt = 0;
        fit_file_t * f;
        f = FIT_open(argv[argc]);
        if (!f) 
            continue;
        printf("FILE: %s\n", argv[argc]);
        puts("idx\tLAT\tLONG\n");

        while (1)
        {
            int ret, pos[2];
            double decoded[2];
            ++cnt;
            rec = FIT_readRecord(f, recycle);
            if (NULL == rec)
            {
                break;
            }
            recycle = rec;
            ret = FIT_getValues(rec, FIT_GLOBAL_RECORD,
                    FIT_RECORD_POSITION_LAT_S32, pos,
                    FIT_RECORD_POSITION_LONG_S32, pos +1,
                    -1);
            if (0 == ret)
            {
                printf("%d\t%lf\t%lf\n", cnt, 8.3819e-8 * pos[0], 8.3819e-8 * pos[1]);
                decoded[0] = FIT_getRatio(FIT_GLOBAL_RECORD, FIT_RECORD_POSITION_LAT_S32) * pos[0];
                FIT_decodeValues(rec, FIT_GLOBAL_RECORD, FIT_RECORD_POSITION_LONG_S32, decoded + 1);
                //decoded[1] = FIT_getRatio(FIT_GLOBAL_RECORD, FIT_RECORD_POSITION_LONG_S32) * pos[1];

               printf("%d\t%lf\t%lf\n", cnt, decoded[0], decoded[1]);
            }


        }
        puts("FIT_free");

        FIT_free(recycle);
        puts("");

        puts("FIT_close");
        FIT_close(f);
    }
    return 0;
}
