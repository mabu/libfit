/**/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libfit.h"
#include "libfit-internal.h"

static uint16_t _crc(uint16_t crc, uint8_t byte)
{
    static const uint16_t crc_table[16] =
    {
        0x0000, 0xCC01, 0xD801, 0x1400, 0xF001, 0x3C00, 0x2800, 0xE401,
        0xA001, 0x6C00, 0x7800, 0xB401, 0x5000, 0x9C01, 0x8801, 0x4400
    };

    uint16_t tmp;

    // compute checksum of lower four bits of byte
    tmp = crc_table[crc & 0xF];
    crc = (crc >> 4) & 0x0FFF;
    crc = crc ^ tmp ^ crc_table[byte & 0xF];

    // now compute checksum of upper four bits of byte
    tmp = crc_table[crc & 0xF];
    crc = (crc >> 4) & 0x0FFF;
    crc = crc ^ tmp ^ crc_table[(byte >> 4) & 0xF];

    return crc;
}

static uint16_t compute_crc(uint8_t *data, size_t size)
{
    uint16_t crc = 0;
    uint8_t *end = data + size;
    while(data != end) {
        crc = _crc(crc, *data++);
    }
    return crc;
}

static uint16_t compute_file_crc(FILE *f)
{
    uint16_t crc = 0;
    long size, pos;

    pos = ftell(f);
    fseek(f, 0, SEEK_END);
    size = ftell(f);
    rewind(f);
    if (size < 2) {
        goto EXIT;
    }

    size -= 2;

    uint8_t * buff = malloc(size);

    if (buff) {
        fread(buff, 1, size, f);
        crc = compute_crc(buff, size);
        free(buff);
    } else {
        for (int i = 0; i < size; ++i) {
            crc = _crc(crc, fgetc(f));
        }
    }

EXIT:
    fseek(f, pos, SEEK_SET);
    return crc;
}

static int isHeaderValid(FILE *f)
{
    fit_header_t header;
    rewind(f);
    if (1 != fread(&header, sizeof header, 1, f))
    {
        LOG("Cannot read header...");
        return 0;
    }
    LOG_DEBUG("Header size:      %d", header.size);
    LOG_DEBUG("       version:   %d", header.proto_version);
    LOG_DEBUG("       profile:   %d", header.profile_version);
    LOG_DEBUG("       data size: %d", header.data_size);
    LOG_DEBUG("       type:      %c%c%c%c", header.type[0], header.type[1], header.type[2], header.type[3]);
    LOG_DEBUG("       CRC:       0x%04x", header.CRC);

    if (memcmp(header.type, ".FIT", 4)) {
        LOG("Error: type is not '.FIT'");
        return 0;
    }

    /** Check header CRC */
    uint16_t crc = compute_crc((uint8_t*)&header, sizeof header - sizeof(uint16_t));

    LOG_DEBUG("Computed CRC:    0x%04x", crc);

    if (header.CRC && (header.CRC != crc)) {
        LOG("CRC header incorrect!");
        return 0;
    }

    /** check file CRC */
    long pos = ftell(f);

    fseek(f, -2, SEEK_END);
    uint16_t file_crc;

    fread(&file_crc, sizeof file_crc, 1, f);

    fseek(f, pos, SEEK_SET);


    crc = compute_file_crc(f);
    LOG_DEBUG("Computed CRC: 0x%04x", crc);
    LOG_DEBUG("File CRC:     0x%04x", file_crc);

    if (header.CRC && (file_crc != crc)) {
        LOG("CRC file incorrect!");
        return 0;
    }

    return 1;
}

fit_file_t * FIT_open(const char *name)
{
    fit_file_t *ret = NULL;
    FILE *f = NULL;

    f = fopen(name, "rb");

    if (NULL == f)
    {
        LOG_DEBUG("%s opening failed!\n", name);
        goto ERROR;
    }

    if (!isHeaderValid(f))
    {
        LOG_DEBUG("invalid header!\n");
        goto ERROR;
    }

    ret = calloc(1, sizeof *ret);

    if (NULL == ret)
    {
        perror("calloc");
        goto ERROR;
    }

    ret->file = f;

    return ret;

ERROR:
    if (f)
        fclose(f);
    free(ret);
    return NULL;
}
