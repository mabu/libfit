#include <stdio.h>
#ifdef _WIN32
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
#else
#include <stdint.h>
#endif

#ifndef H_20171008_LIBFIT
#define H_20171008_LIBFIT

typedef struct fit_file fit_file_t;
typedef struct fit_def fit_def_t;
typedef struct fit_record fit_record_t;
typedef struct fit_normal_header fit_normal_header_t;

struct fit_file
{
    FILE * file;
    fit_def_t * defs;
    int defs_cnt;
};

struct fit_field_info
{
    int name; //char name[32];
    int size;
    int type;
};

/* functions */
/* Open a fit file in read mode
 * return NULL on error
 */
fit_file_t* FIT_open(const char *name);

/*
 * Release resources acquired by FIT_open
 */
void FIT_close(fit_file_t*);

/* Release resources acquired by FIT_readRecord */
void FIT_free(fit_record_t*);

/* Read next data record (definition record are not returned but
 * they update the internal base
 * f : pointer returned by FIT_open
 * recycle : NULL or a pointer precedently returned by this function (to avoid free/allocation cycle)
 * return NULL on error*/
fit_record_t* FIT_readRecord(fit_file_t *f, fit_record_t *recycle);

/* Return the record type (FIT_GLOBAL_FILE_ID, FIT_GLOBAL_RECORD, FIT_GLOBAL...) */
int FIT_getRecordType(fit_record_t *);

/* Get string name for given type
 * params
 * name: memory to store the result
 * size: sizeof name
 * type: type to be decoded
 */
int FIT_decodeRecordType(char *name, size_t size, int type);

/*
 * params
 * name: memory to store the result
 * size: sizeof name
 * type: type to be decoded
 * enum_type:
 * enum_value: enum value to be decoded
 */
int FIT_decodeEnum(char *name, size_t size, int type, int enum_type, int enum_value);

/* Get a pointer on the Nth values of the given record */
void * FIT_getValue(fit_record_t *, int N);

/* get a raw value
 * params
 * r: record to read
 * type: the record type (FIT_GLOBAL_SESSION, FIT_GLOBAL_RECORD, FIT_GLOBAL...)
 * and for each value to read:
 * field: the wanted field (FIT_SESSION_START_TIME_U32, FIT_RECORD_POSITION_LAT_S32, ...)
 * p: a pointer on memory to store the raw values (take care of type)
 *
 * And end the parameter list with -1
 * @return number of values got
 */
int FIT_getValues(fit_record_t *r, int type, ...);

/* get decoded value
 * Same as FIT_getValues, but values are converted to `double` type.
 * @return number of values got
 * */
int FIT_decodeValues(fit_record_t *, int, ...);

/* Get conversion ratio for certain type
 * msg: record type (FIT_GLOBAL_RECORD...)
 * type: field type (FIT_RECORD_POSITION_LAT_S32...)
 * */
double FIT_getRatio(int msg, int type);

/* Get the definition of the given record */
fit_def_t * FIT_getDefinition(fit_record_t *rec);

/* Get number of fields in definition */
int FIT_getFieldsCount(fit_def_t *def);

/* Get field information */
int FIT_getFieldInfo(fit_def_t *def, int idx, struct fit_field_info *info);

const char *FIT_getFieldName(fit_def_t *def, int idx);
const char *FIT_getFieldType(fit_def_t *def, int idx);
const char *FIT_info(void);

#endif

