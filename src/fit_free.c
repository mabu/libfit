#include <stdlib.h>
#include "libfit.h"
#include "libfit-internal.h"

void FIT_free(fit_record_t *rec)
{
    LOG_DEBUG("FIT_free(%p)", rec);
    if (rec) {
        LOG_DEBUG("free(%p)", rec->data);
        free(rec->data);
        LOG_DEBUG("free(%p)", rec);
        free(rec);
    }
}

