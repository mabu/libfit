#include <stdlib.h>
#include "libfit.h"
#include "libfit-internal.h"

void FIT_close(fit_file_t *f)
{
    if (f && f->file)
    {
        // free defs : fit_def_t
        fclose(f->file);
        LOG_DEBUG("%d definitions to free\n", f->defs_cnt);
        for (int i = 0; i < f->defs_cnt; ++i)
            free(f->defs[i].fields);
        free(f->defs);
        free(f);
    }
}

