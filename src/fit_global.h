#ifndef H_20171116_FIT_GLOBAL
#define H_20171116_FIT_GLOBAL

enum global_message 
{
    file_id,
    capabilities,
    device_settings,
    user_profile,
    hrm_profile,
    sdm_profile,	
    bike_profile,
    zones_target,
    hr_zone,
    power_zone,
    met_zone,
    sport	= 12,
    goal	= 15,
    session	= 18,
    lap,
    record,
    event,
    device_info	= 23,
    file_creator = 49,
};

#endif
