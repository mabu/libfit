#include "libfit-internal.h"
#include "libfit-messages.h"

fit_def_t * FIT_getDefinition(fit_record_t *rec)
{
    if (rec)
        return rec->definition;
    return NULL;
}

int FIT_getFieldsCount(fit_def_t *def)
{
    if (def)
        return def->fields_count;
    return 0;
}

static char s_mem[64];

const char *FIT_getFieldName(fit_def_t *def, int idx)
{
    if (!def || idx < 0 ||idx >= def->fields_count)
    {
        return NULL;
    }

    switch (def->index)
    {
        case FIT_GLOBAL_FILE_ID:
            switch (def->fields[idx].field_definition_num)
            {
                case FIT_FILE_TYPE          : return "TYPE";
                case FIT_FILE_MANUFACTURER  : return "MANUFACTURER";
                case FIT_FILE_PRODUCT       : return "PRODUCT";
                case FIT_FILE_SERIAL_NUMBER : return "SERIAL_NUMBER";
                case FIT_FILE_TIME_STAMP    : return "TIME_STAMP";
                case FIT_FILE_NUMBER        : return "NUMBER";
            }
            break;

        case FIT_GLOBAL_FILE_CREATOR:
            switch (def->fields[idx].field_definition_num)
            {
                case FIT_FILE_CREATOR_SOFTWARE_VERSION_U16 : return "SOFTWARE_VERSION";
                case FIT_FILE_CREATOR_HARDWARE_VERSION_U8  : return "HARDWARE_VERSION";
            }
            break;

        case FIT_GLOBAL_RECORD:
            switch (def->fields[idx].field_definition_num)
            {
                case FIT_RECORD_POSITION_LAT_S32: return "POSITION_LAT_S32";
                case FIT_RECORD_POSITION_LONG_S32: return "POSITION_LONG_S32";
                case FIT_RECORD_DISTANCE_U32: return "DISTANCE_U32";
                case FIT_RECORD_SPEED_U16: return "SPEED_U16";
                case FIT_RECORD_TIMESTAMP_U32: return "TIMESTAMP_U32";
                case FIT_RECORD_HEART_RATE_U8: return "HEART_RATE";
                case FIT_RECORD_ALTITUDE_U16: return "ALTITUDE";
                case FIT_RECORD_CALORIES: return "CALORIES";
            }
            break;

        case FIT_GLOBAL_SESSION:
            switch (def->fields[idx].field_definition_num)
            {
                case FIT_SESSION_TIMESTAMP_U32 : return "TIMESTAMP_U32";
                case FIT_SESSION_START_TIME_U32 : return "START_TIME_U32";
                case FIT_SESSION_START_POSITION_LAT_S32 : return "START_POSITION_LAT_S32";
                case FIT_SESSION_START_POSITION_LONG_S32 : return "START_POSITION_LONG_S32";
                case FIT_SESSION_TOTAL_ELAPSED_TIME_U32 : return "TOTAL_ELAPSED_TIME_U32";
                case FIT_SESSION_TOTAL_TIMER_TIME_U32 : return "TOTAL_TIMER_TIME_U32";
                case FIT_SESSION_TOTAL_DISTANCE_U32 : return "TOTAL_DISTANCE_U32";
                case FIT_SESSION_MESSAGE_INDEX_U16 : return "MESSAGE_INDEX_U16";
                case FIT_SESSION_TOTAL_CALORIES_U16 : return "TOTAL_CALORIES_U16";
                case FIT_SESSION_AVG_SPEED_U16 : return "AVG_SPEED_U16";
                case FIT_SESSION_FIRST_LAP_INDEX_U16 : return "FIRST_LAP_INDEX_U16";
                case FIT_SESSION_NUM_LAPS_U16 : return "NUM_LAPS_U16";
                case FIT_SESSION_EVENT_ENUM : return "EVENT_ENUM";
                case FIT_SESSION_EVENT_TYPE_ENUM : return "EVENT_TYPE_ENUM";
                case FIT_SESSION_SPORT_ENUM : return "SPORT_ENUM";
                case FIT_SESSION_SUB_SPORT_ENUM : return "SUB_SPORT_ENUM";
                case FIT_SESSION_AVG_HEART_RATE : return "AVG_HEART_RATE";
                case FIT_SESSION_MAX_HEART_RATE : return "MAX_HEART_RATE";
                case FIT_SESSION_MIN_HEART_RATE : return "MIN_HEART_RATE";

               case FIT_SESSION_TRIGGER_ENUM : return "TRIGGER_ENUM";
            }
            break;

        case FIT_GLOBAL_COURSE_POINT:
            switch (def->fields[idx].field_definition_num)
            {
                case FIT_COURSE_POINT_GENERIC : return "GENERIC";
                case FIT_COURSE_POINT_TIMESTAMP : return "TIMESTAMP";
                case FIT_COURSE_POINT_POSITION_LAT : return "POSITION_LAT";
                case FIT_COURSE_POINT_POSITION_LONG : return "POSITION_LONG";
                case FIT_COURSE_POINT_DISTANCE : return "DISTANCE";
                case FIT_COURSE_POINT_NAME : return "NAME";
                case FIT_COURSE_POINT_MESSAGE_INDEX : return "MESSAGE_INDEX";
                case FIT_COURSE_POINT_TYPE : return "TYPE";
                case FIT_COURSE_POINT_FAVORITE : return "FAVORITE";
            }
            break;

        case FIT_GLOBAL_MONITORING:
            switch (def->fields[idx].field_definition_num)
            {
                case FIT_MONITORING_TIMESTAMP : return "TIMESTAMP";
                case FIT_MONITORING_DEVICE_INDEX : return "DEVICE_INDEX";
                case FIT_MONITORING_CALORIES : return "CALORIES";
                case FIT_MONITORING_DISTANCE : return "DISTANCE";
                case FIT_MONITORING_CYCLES : return "CYCLES";
                case FIT_MONITORING_ACTIVE_TIME : return "ACTIVE_TIME";
                case FIT_MONITORING_ACTIVITY_TYPE : return "ACTIVITY_TYPE";
                case FIT_MONITORING_ACTIVITY_SUBTYPE : return "ACTIVITY_SUBTYPE";
                case FIT_MONITORING_DISTANCE_16 : return "DISTANCE_16";
                case FIT_MONITORING_CYCLES_16 : return "CYCLES_16";
                case FIT_MONITORING_ACTIVE_TIME_16 : return "ACTIVE_TIME_16";
                case FIT_MONITORING_DURATION_MIN : return "DURATION_MIN";
                case FIT_MONITORING_DURATION_SEC : return "DURATION_SEC";
                case FIT_MONITORING_LOCAL_TIMESTAMP : return "LOCAL_TIMESTAMP";
            }
            break;

        case FIT_GLOBAL_EVENT:
            switch (def->fields[idx].field_definition_num)
            {
                case FIT_EVENT_TIMESTAMP_U32 : return "TIMESTAMP_U32";
                case FIT_EVENT_TIMER : return "TIMER";
                case FIT_EVENT_TYPE : return "TYPE";
                case FIT_EVENT_DATA_16 : return "DATA_16";
                case FIT_EVENT_WORKOUT : return "WORKOUT";
                case FIT_EVENT_WORKOUT_STEP : return "WORKOUT_STEP";
                case FIT_EVENT_HR_HIGH_ALERT : return "HR_HIGH_ALERT";
                case FIT_EVENT_HR_LOW_ALERT : return "HR_LOW_ALERT";
                case FIT_EVENT_SPEED_HIGH_ALERT : return "SPEED_HIGH_ALERT";
                case FIT_EVENT_POWER_HIGH_ALERT : return "POWER_HIGH_ALERT";
                case FIT_EVENT_POWER_LOW_ALERT : return "POWER_LOW_ALERT";
            }
            break;


        case FIT_GLOBAL_DEVICE_INFO:
            switch (def->fields[idx].field_definition_num)
            {
                case FIT_DEVICE_INFO_DEVICE_INDEX : return "DEVICE_INDEX";
                case FIT_DEVICE_INFO_DEVICE_TYPE : return "DEVICE_TYPE";
                case FIT_DEVICE_INFO_MANUFACTURER : return "MANUFACTURER";
                case FIT_DEVICE_INFO_SERIAL_NUMBER : return "SERIAL_NUMBER";
                case FIT_DEVICE_INFO_PRODUCT : return "PRODUCT";
                case FIT_DEVICE_INFO_SOFTWARE_VERSION : return "SOFTWARE_VERSION";
                case FIT_DEVICE_INFO_HARDWARE_VERSION : return "HARDWARE_VERSION";
                case FIT_DEVICE_INFO_CUM_OPERATING_TIME : return "CUM_OPERATING_TIME";
                case FIT_DEVICE_INFO_BATTERY_VOLTAGE : return "BATTERY_VOLTAGE";
                case FIT_DEVICE_INFO_BATTERY_STATUS : return "BATTERY_STATUS";
                case FIT_DEVICE_INFO_SENSOR_POSITION : return "SENSOR_POSITION";
                case FIT_DEVICE_INFO_DESCRIPTOR : return "DESCRIPTOR";
                case FIT_DEVICE_INFO_ANT_TRANSMISSION_TYPE : return "ANT_TRANSMISSION_TYPE";
                case FIT_DEVICE_INFO_ANT_DEVICE_NUMBER : return "ANT_DEVICE_NUMBER";
                case FIT_DEVICE_INFO_ANT_NETWORK : return "ANT_NETWORK";
                case FIT_DEVICE_INFO_SOURCE_TYPE : return "SOURCE_TYPE";
                case FIT_DEVICE_INFO_PRODUCT_NAME : return "PRODUCT_NAME";
                case FIT_DEVICE_INFO_TIMESTAMP : return "TIMESTAMP";
            }
            break;

        case FIT_GLOBAL_LAP:
            switch (def->fields[idx].field_definition_num)
            {
                case FIT_LAP_TIMESTAMP : return "TIMESTAMP";
                case FIT_LAP_EVENT : return "EVENT";
                case FIT_LAP_EVENT_TYPE : return "EVENT_TYPE";
                case FIT_LAP_START_TIME : return "START_TIME";
                case FIT_LAP_START_POSITION_LAT : return "START_POSITION_LAT";
                case FIT_LAP_START_POSITION_LON : return "START_POSITION_LON";
                case FIT_LAP_END_POSITION_LAT : return "END_POSITION_LAT";
                case FIT_LAP_END_POSITION_LON : return "END_POSITION_LON";
                case FIT_LAP_ELAPSED_TIME : return "ELAPSED_TIME";
                case FIT_LAP_TIMER_TIME : return "TIMER_TIME";
                case FIT_LAP_DISTANCE : return "DISTANCE";
                case FIT_LAP_CYCLES : return "CYCLES";
                case FIT_LAP_CALORIES : return "CALORIES";
                case FIT_LAP_FAT_CALORIES : return "FAT_CALORIES";
                case FIT_LAP_AVG_SPEED : return "AVG_SPEED";
                case FIT_LAP_MAX_SPEED : return "MAX_SPEED";
                case FIT_LAP_AVG_HR : return "AVG_HR";
                case FIT_LAP_MAX_HR : return "MAX_HR";
            }
            break;

        case FIT_GLOBAL_NOT_DOC_140:
            switch (def->fields[idx].field_definition_num)
            {
                case FIT_NOT_DOC_VO2MAX : return "VO2MAX";
            }
            break;

        case FIT_GLOBAL_USER_DATA:
            switch (def->fields[idx].field_definition_num)
            {
                //case FIT_USER_DATA_VO2_MAX : return "VO2MAX";
                case FIT_USER_DATA_AGE : return "AGE";
                case FIT_USER_DATA_HEIGHT : return "HEIGHT";
                case FIT_USER_DATA_WEIGHT : return "WEIGHT";
                case FIT_USER_DATA_GENDER : return "GENDER";
            }
            break;

    }
        //LOG_DEBUG("Not implemented for %d / %d\n", def->index, def->fields[idx].field_definition_num);
        //default: return "not implemented";
        //default:
        //{
            snprintf(s_mem, sizeof s_mem, "%d/%d, not implemented",  def->index, def->fields[idx].field_definition_num);
            return s_mem;
        //}
    //}
}

const char *FIT_getFieldType(fit_def_t *def, int idx)
{
    if (!def || idx < 0 ||idx >= def->fields_count)
    {
        return NULL;
    }
    switch (def->fields[idx].base_type)
    {
        case FIT_TYPE_enum: return "enum";
        case FIT_TYPE_int8: return "int8";
        case FIT_TYPE_uint8: return "uint8";
        case FIT_TYPE_int16: return "int16";
        case FIT_TYPE_uint16: return "uint16";
        case FIT_TYPE_int32: return "int32";
        case FIT_TYPE_uint32: return "uint32";
        case FIT_TYPE_string: return "string";
        case FIT_TYPE_float: return "float";
        case FIT_TYPE_double: return "double";
        case FIT_TYPE_uint8z: return "uint8z";
        case FIT_TYPE_uint16z: return "uint16z";
        case FIT_TYPE_uint32z: return "uint32z";
        case FIT_TYPE_bytes: return "bytes";
        case FIT_TYPE_int64: return "int64";
        case FIT_TYPE_uint64: return "uint64";
        case FIT_TYPE_uint64z: return "uint64z";
        default : return "?";
    }
}


int FIT_getFieldInfo(fit_def_t *def, int idx, struct fit_field_info *info)
{
    if (def && info && idx >= 0 && idx < def->fields_count)
    {
        info->size = def->fields[idx].size;
        info->type = def->fields[idx].base_type;
        info->name = def->fields[idx].field_definition_num;
        return 0;
    }
    return -1;
}
