#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "libfit.h"
#include "libfit-messages.h"
#include "libfit-internal.h"

#ifndef NDEBUG
static const char * type_name(int type)
{
    switch (type)
    {
        case 0:
            return "enum";
        case 1:
            return "int8_t";
        case 2:
            return "uint8_t";
        case 0x83:
            return "int16_t";
        case 0x84:
            return "uint16_t";
        case 0x85:
            return "int32_t";
        case 0x86:
            return "uint32_t";
        case 7:
            return "string";
        case 0x88:
            return "float";
        case 0x89:
            return "double";
        case 0x0A:
            return "uint8z_t";
        case 0x8B:
            return "uint16z_t";
        case 0x8C:
            return "uint32z_t";
        case 0x0D:
            return "bytes";
        case 0x8E:
            return "int64_t";
        case 0x8F:
            return "uint64_t";
        case 0x90:
            return "uint64z_t";
        default :
            return "?";
    }
}
#endif

void readDefinition(fit_file_t *f, int localMessageType)
{
    fit_fixed_header_t fix_head;
    fit_def_t *curdef;
    fit_field_def_t *fields;
    int i;

    // add a definition
    int def_index = localMessageType;
    int req_size = def_index + 1;
    if (f->defs_cnt < req_size)
    {
        int oldsize = f->defs_cnt * sizeof(fit_def_t), newsize = req_size*sizeof(fit_def_t);
        f->defs_cnt = req_size;
        f->defs = realloc(f->defs, f->defs_cnt * sizeof(fit_def_t));
        memset((char*)f->defs + oldsize, 0, newsize - oldsize);
        LOG_DEBUG("READ DEF: allocated %d (was %d)", newsize, oldsize);
        LOG_DEBUG("clear %d bytes", newsize - oldsize );
    }
    if (NULL == f->defs)
    {
        LOG("realloc: '%s' allocating %lu bytes", strerror(errno), f->defs_cnt * sizeof(fit_def_t));
        perror("realloc");
        exit(1);
    }

    curdef = &f->defs[def_index];

    if (1 != fread(&fix_head, sizeof fix_head, 1, f->file))
    {
        if (!feof(f->file))
        {
            perror("72 - fread");
        }
        return;
    }
    LOG_DEBUG("reading info for def %d", def_index);
    LOG_DEBUG("current was %d at %p", curdef->fields_count, curdef->fields);
    curdef->index = fix_head.message_number;
    curdef->arch = fix_head.arch;
    if (curdef->fields_count)
        free(curdef->fields);

    curdef->fields_count = fix_head.fields_cnt;
    curdef->fields = calloc(fix_head.fields_cnt, sizeof(fit_field_def_t));
    curdef->size = 0;

    fields = curdef->fields;

    LOG_DEBUG(" message_number: %d", fix_head.message_number);
    LOG_DEBUG(" fields: %d", fix_head.fields_cnt);

    for (i = 0; i < fix_head.fields_cnt; ++i)
    {
        if (1 != fread(&fields[i], sizeof *fields, 1, f->file))
        {
            perror("92 - fread");
            break;
        }
        curdef->size += fields[i].size;
        LOG_DEBUG("    definition#: %d", fields[i].field_definition_num);
        LOG_DEBUG("    size:        %d", fields[i].size);
        LOG_DEBUG("    base type:   %d (%s)\n", fields[i].base_type, type_name(fields[i].base_type));
    }
    LOG_DEBUG("fields size: %d\n", curdef->size);
}

fit_record_t *FIT_readRecord(fit_file_t *f, fit_record_t *recycle)
{
    //static int record_cnt = 0, definition_cnt = 0;
    fit_normal_header_t header;
    fit_record_t *ret = recycle;

    if (NULL == ret)
    {
        ret = calloc(1, sizeof *ret);
    }

    if (NULL == ret)
    {
        perror("calloc");
        goto EXIT;
    }

    // if f && f->file
    /* read record header (1 byte) */
    if (1 != fread(&header, sizeof header, 1, f->file))
    {
        if (feof(f->file))
        {
            LOG_DEBUG("read failed: end of file");
        }
        if (NULL == recycle)
        {
            free(ret);
        }
        ret = NULL;
        goto EXIT;
    }

    LOG_DEBUG("local message type:  %d", header.local_message_type);
    LOG_DEBUG("header size: %lu", sizeof header);

    if (TYPE_DEFINITION == header.message_type)
    {

        LOG_DEBUG("-==definition message==-");
        readDefinition(f, header.local_message_type);
        LOG_DEBUG(" definition read ");
        return FIT_readRecord(f, ret);
    }
    else
    {
        fit_def_t *curdef = &f->defs[header.local_message_type];

        ret->definition = curdef;

        LOG_DEBUG("ret is %p", ret);
        LOG_DEBUG("ret->def is %p", ret->definition);
        LOG_DEBUG("ret->data is %p", ret->data);
        LOG_DEBUG("reading message type: %d", header.local_message_type);
        ret->data = realloc(ret->data, curdef->size);

        LOG_DEBUG("ret->data is %p", ret->data);

        if (NULL == ret->data)
        {
            if (errno)
                LOG("realloc: '%s' allocating %d bytes", strerror(errno), curdef->size);
            if (NULL == recycle)
            {
                free(ret);
            }
            ret = NULL;
            goto EXIT;
        }

        int nread = fread(ret->data, 1, curdef->size, f->file);
        if (curdef->size != nread)
        {
            LOG_DEBUG("fread failed(%d/%d), errno is %d", nread, curdef->size, errno);
            if (NULL == recycle)
            {
                free(ret);
            }
            ret = NULL;
            goto EXIT;
        }
    }

EXIT:

    return ret;
}
