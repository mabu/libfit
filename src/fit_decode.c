#include <stdlib.h>
#include <string.h>
#include "libfit-internal.h"
#include "libfit-messages.h"
#include <stdarg.h>
#include <math.h>


void * FIT_getValue(fit_record_t * record, int idx)
{
    fit_field_def_t * fields;
    if (!record || idx < 0 || idx >= record->definition->fields_count) {
        return NULL;
    }

    fields = record->definition->fields;

    int pos = 0, i;

    for (i = 0; i < idx; ++i) {
        pos += fields[i].size;
    }

    return record->data + pos;

}

int FIT_getValues(fit_record_t * record, int type, ...)
{
    int ret = 0;
    va_list ap;
    int what, i;
    fit_field_def_t *fields;

    if (!record || (type != record->definition->index))
        return ret;

    va_start(ap, type);
    fields = record->definition->fields;

    if (!fields)
        return ret;

    while (1) {
        void *where;
        int pos = 0, found = 0;
        what = va_arg(ap, int);
        LOG_DEBUG("what is %d", what);
        if (-1 == what) {
            break;
        }
        where = va_arg(ap, void *);

        /* search for field in definition */
        for (i = 0; i < record->definition->fields_count; ++i) {
            if (what == fields[i].field_definition_num) {
                /* found */
                memcpy(where, record->data + pos, fields[i].size);
                found = 1;
                ret ++;
                break;
            } else {
                pos += fields[i].size;
            }
        }
        if (!found) {
            LOG_DEBUG("Error: field %d not found!", what);
            break;
        }

    }

    va_end(ap);

    return ret;
}

static double decode(void *data, int type, int what)
{
    double ret = 0;
    LOG_DEBUG("%s(%p, %d, %d)", __FUNCTION__, data, type, what);

    switch (type) {
    case FIT_GLOBAL_NOT_DOC_140:
        switch (what) {
            case FIT_NOT_DOC_VO2MAX:
                return 3.5 / 65536.0 * (double) *(int *)data;
        }
        break;
    case FIT_GLOBAL_RECORD:
        switch (what) {
        case FIT_RECORD_POSITION_LAT_S32:
        case FIT_RECORD_POSITION_LONG_S32:
            /* 180 / 1<<31 */
            return 8.381903171539306640625e-8 * (double) *(int *) data;
        case FIT_RECORD_DISTANCE_U32:
            return .01 * (double) *(unsigned int *) data;
        case FIT_RECORD_SPEED_U16:
            return 36. / 10000. * (double) *(unsigned short *) data;
        case FIT_RECORD_HEART_RATE_U8:
            return 1. * (double) *(unsigned char *) data;
        case FIT_RECORD_ALTITUDE_U16:
            return - 500. + .2 * (double) *(unsigned short *) data;
        default:
            {
                LOG("implement me!");
            }
        }
        break;
    case FIT_GLOBAL_USER_DATA:
        switch (what){
            case FIT_USER_DATA_AGE:
            case FIT_USER_DATA_GENDER:
            case FIT_USER_DATA_HEIGHT:
                return (double) *(unsigned char *) data;
            case FIT_USER_DATA_WEIGHT:
                return .1*(double) *(unsigned short *) data;

        }
    case FIT_GLOBAL_SESSION:
        switch (what){
        case FIT_SESSION_TIMESTAMP_U32:
        case FIT_SESSION_START_TIME_U32:
        case FIT_SESSION_START_POSITION_LAT_S32:
        case FIT_SESSION_START_POSITION_LONG_S32:
        case FIT_SESSION_TOTAL_ELAPSED_TIME_U32:
        case FIT_SESSION_TOTAL_TIMER_TIME_U32:
            return .001 * (double) *(unsigned int *) data;
        case FIT_SESSION_TOTAL_DISTANCE_U32:
            return .01 * (double) *(unsigned int *) data;
        case FIT_SESSION_MESSAGE_INDEX_U16:
        case FIT_SESSION_TOTAL_CALORIES_U16:
            return (double) *(short *) data;
        case FIT_SESSION_AVG_SPEED_U16:
        case FIT_SESSION_FIRST_LAP_INDEX_U16:
        case FIT_SESSION_NUM_LAPS_U16:
        case FIT_SESSION_EVENT_ENUM:
        case FIT_SESSION_EVENT_TYPE_ENUM:
        case FIT_SESSION_SPORT_ENUM:
            LOG("implement me!");
        }
        break;
    }
    return ret;

}

int FIT_decodeValues(fit_record_t * record, int type, ...)
{
    int ret = 0;
    va_list ap;
    int what, i;
    fit_field_def_t *fields;

    if (type != record->definition->index) {
        return ret;
    }

    va_start(ap, type);
    fields = record->definition->fields;

    while (1) {
        double *where;
        int pos = 0, found = 0;
        what = va_arg(ap, int);
        LOG_DEBUG("what is %d", what);
        if (-1 == what) {
            break;
        }
        where = (double *) va_arg(ap, void *);

        /* search for field in definition */

        for (i = 0; i < record->definition->fields_count; ++i) {
            //LOG_DEBUG("%d, type: %d, size: %d, pos: %d", i, fields[i].field_definition_num, fields[i].size, pos);
            if (what == fields[i].field_definition_num) {
                /* found */
                void *data = malloc(fields[i].size);
                memcpy(data, record->data + pos, fields[i].size);
                /* convert data to double */
                *where = decode(data, type, what);
                found = 1;
                ret ++;
                free(data);
                break;
            } else {
                pos += fields[i].size;
            }
        }
        if (!found) {
            LOG_DEBUG("Error: field %d not found!", what);
            *where = NAN;
        }

    }

    va_end(ap);

    return ret;
}

int FIT_getRecordType(fit_record_t * record)
{
    return record->definition->index;
}


int FIT_decodeRecordType(char *name, size_t size, int type)
{
    int ret = 0;

    switch (type) {
    case FIT_GLOBAL_FILE_ID:
        strncpy(name, "FIT_GLOBAL_FILE_ID", size);
        break;
    case FIT_GLOBAL_CAPABILITIES:
        strncpy(name, "FIT_GLOBAL_CAPABILITIES", size);
        break;
    case FIT_GLOBAL_DEVICE_SETTINGS:
        strncpy(name, "FIT_GLOBAL_DEVICE_SETTINGS", size);
        break;
    case FIT_GLOBAL_USER_DATA:
        strncpy(name, "FIT_GLOBAL_USER_DATA", size);
        break;
    case FIT_GLOBAL_USER_PROFILE:
        strncpy(name, "FIT_GLOBAL_USER_PROFILE", size);
        break;
    case FIT_GLOBAL_HRM_PROFILE:
        strncpy(name, "FIT_GLOBAL_HRM_PROFILE", size);
        break;
    case FIT_GLOBAL_SDM_PROFILE:
        strncpy(name, "FIT_GLOBAL_SDM_PROFILE", size);
        break;
    case FIT_GLOBAL_BIKE_PROFILE:
        strncpy(name, "FIT_GLOBAL_BIKE_PROFILE", size);
        break;
    case FIT_GLOBAL_ZONES_TARGET:
        strncpy(name, "FIT_GLOBAL_ZONES_TARGET", size);
        break;
    case FIT_GLOBAL_HR_ZONE:
        strncpy(name, "FIT_GLOBAL_HR_ZONE", size);
        break;
    case FIT_GLOBAL_POWER_ZONE:
        strncpy(name, "FIT_GLOBAL_POWER_ZONE", size);
        break;
    case FIT_GLOBAL_MET_ZONE:
        strncpy(name, "FIT_GLOBAL_MET_ZONE", size);
        break;
    case FIT_GLOBAL_SPORT:
        strncpy(name, "FIT_GLOBAL_SPORT", size);
        break;
    case FIT_GLOBAL_GOAL:
        strncpy(name, "FIT_GLOBAL_GOAL", size);
        break;
    case FIT_GLOBAL_SESSION:
        strncpy(name, "FIT_GLOBAL_SESSION", size);
        break;
    case FIT_GLOBAL_LAP:
        strncpy(name, "FIT_GLOBAL_LAP", size);
        break;
    case FIT_GLOBAL_RECORD:
        strncpy(name, "FIT_GLOBAL_RECORD", size);
        break;
    case FIT_GLOBAL_EVENT:
        strncpy(name, "FIT_GLOBAL_EVENT", size);
        break;
    case FIT_GLOBAL_DEVICE_INFO:
        strncpy(name, "FIT_GLOBAL_DEVICE_INFO", size);
        break;
    case FIT_GLOBAL_WORKOUT:
        strncpy(name, "FIT_GLOBAL_WORKOUT", size);
        break;
    case FIT_GLOBAL_WORKOUT_STEP:
        strncpy(name, "FIT_GLOBAL_WORKOUT_STEP", size);
        break;
    case FIT_GLOBAL_SCHEDULE:
        strncpy(name, "FIT_GLOBAL_SCHEDULE", size);
        break;
    case FIT_GLOBAL_WEIGHT_SCALE:
        strncpy(name, "FIT_GLOBAL_WEIGHT_SCALE", size);
        break;
    case FIT_GLOBAL_COURSE:
        strncpy(name, "FIT_GLOBAL_COURSE", size);
        break;
    case FIT_GLOBAL_COURSE_POINT:
        strncpy(name, "FIT_GLOBAL_COURSE_POINT", size);
        break;
    case FIT_GLOBAL_TOTALS:
        strncpy(name, "FIT_GLOBAL_TOTALS", size);
        break;
    case FIT_GLOBAL_ACTIVITY:
        strncpy(name, "FIT_GLOBAL_ACTIVITY", size);
        break;
    case FIT_GLOBAL_SOFTWARE:
        strncpy(name, "FIT_GLOBAL_SOFTWARE", size);
        break;
    case FIT_GLOBAL_FILE_CAP:
        strncpy(name, "FIT_GLOBAL_FILE_CAP", size);
        break;
    case FIT_GLOBAL_MESG_CAP:
        strncpy(name, "FIT_GLOBAL_MESG_CAP", size);
        break;
    case FIT_GLOBAL_FIELD_CAP:
        strncpy(name, "FIT_GLOBAL_FIELD_CAP", size);
        break;
    case FIT_GLOBAL_FILE_CREATOR:
        strncpy(name, "FIT_GLOBAL_FILE_CREATOR", size);
        break;
    case FIT_GLOBAL_BLOOD_PRESSURE:
        strncpy(name, "FIT_GLOBAL_BLOOD_PRESSURE", size);
        break;
    case FIT_GLOBAL_SPEED_ZONE:
        strncpy(name, "FIT_GLOBAL_SPEED_ZONE", size);
        break;
    case FIT_GLOBAL_MONITORING:
        strncpy(name, "FIT_GLOBAL_MONITORING", size);
        break;
    case FIT_GLOBAL_TRAINING_FILE:
        strncpy(name, "FIT_GLOBAL_TRAINING_FILE", size);
        break;
    case FIT_GLOBAL_HRV:
        strncpy(name, "FIT_GLOBAL_HRV", size);
        break;
    case FIT_GLOBAL_LENGTH:
        strncpy(name, "FIT_GLOBAL_LENGTH", size);
        break;
    case FIT_GLOBAL_MONITORING_INFO:
        strncpy(name, "FIT_GLOBAL_MONITORING_INFO", size);
        break;
    case FIT_GLOBAL_PAD:
        strncpy(name, "FIT_GLOBAL_PAD", size);
        break;
    case FIT_GLOBAL_SLAVE_DEVICE:
        strncpy(name, "FIT_GLOBAL_SLAVE_DEVICE", size);
        break;
    case FIT_GLOBAL_CADENCE_ZONE:
        strncpy(name, "FIT_GLOBAL_CADENCE_ZONE", size);
        break;
    case FIT_GLOBAL_SEGMENT_LAP:
        strncpy(name, "FIT_GLOBAL_SEGMENT_LAP", size);
        break;
    case FIT_GLOBAL_MEMO_GLOB:
        strncpy(name, "FIT_GLOBAL_MEMO_GLOB", size);
        break;
    case FIT_GLOBAL_SEGMENT_ID:
        strncpy(name, "FIT_GLOBAL_SEGMENT_ID", size);
        break;
    case FIT_GLOBAL_SEGMENT_LB_ENT:
        strncpy(name, "FIT_GLOBAL_SEGMENT_LB_ENT", size);
        break;
    case FIT_GLOBAL_SEGMENT_POINT:
        strncpy(name, "FIT_GLOBAL_SEGMENT_POINT", size);
        break;
    case FIT_GLOBAL_SEGMENT_FILE:
        strncpy(name, "FIT_GLOBAL_SEGMENT_FILE", size);
        break;
    case FIT_GLOBAL_GPS_METADATA:
        strncpy(name, "FIT_GLOBAL_GPS_METADATA", size);
        break;
    case FIT_GLOBAL_CAMERA_EVENT:
        strncpy(name, "FIT_GLOBAL_CAMERA_EVENT", size);
        break;
    case FIT_GLOBAL_TIMESTAMP_CORRELATION:
        strncpy(name, "FIT_GLOBAL_TIMESTAMP_CORRELATION", size);
        break;
    case FIT_GLOBAL_GYROSCOPE_DATA:
        strncpy(name, "FIT_GLOBAL_GYROSCOPE_DATA", size);
        break;
    case FIT_GLOBAL_ACCELEROMETER_DATA:
        strncpy(name, "FIT_GLOBAL_ACCELEROMETER_DATA", size);
        break;
    case FIT_GLOBAL_THREE_D_SENSOR_CALIBRATION:
        strncpy(name, "FIT_GLOBAL_THREE_D_SENSOR_CALIBRATION", size);
        break;
    case FIT_GLOBAL_VIDEO_FRAME:
        strncpy(name, "FIT_GLOBAL_VIDEO_FRAME", size);
        break;
    case FIT_GLOBAL_OBDII_DATA:
        strncpy(name, "FIT_GLOBAL_OBDII_DATA", size);
        break;
    case FIT_GLOBAL_NMEA_SENTENCE:
        strncpy(name, "FIT_GLOBAL_NMEA_SENTENCE", size);
        break;
    case FIT_GLOBAL_AVIATION_ATTITUDE:
        strncpy(name, "FIT_GLOBAL_AVIATION_ATTITUDE", size);
        break;
    case FIT_GLOBAL_VIDEO:
        strncpy(name, "FIT_GLOBAL_VIDEO", size);
        break;
    case FIT_GLOBAL_VIDEO_TITLE:
        strncpy(name, "FIT_GLOBAL_VIDEO_TITLE", size);
        break;
    case FIT_GLOBAL_VIDEO_DESCRIPTION:
        strncpy(name, "FIT_GLOBAL_VIDEO_DESCRIPTION", size);
        break;
    case FIT_GLOBAL_VIDEO_CLIP:
        strncpy(name, "FIT_GLOBAL_VIDEO_CLIP", size);
        break;
    case FIT_GLOBAL_MFG_RANGE_MIN:
        strncpy(name, "FIT_GLOBAL_MFG_RANGE_MIN", size);
        break;
    case FIT_GLOBAL_MFG_RANGE_MAX:
        strncpy(name, "FIT_GLOBAL_MFG_RANGE_MAX", size);
        break;
    default:
        ret = 1;
    }

    return ret;
}

int FIT_decodeEnum(char *name, size_t size, int type, int enum_type, int enum_value) {
    switch (type) {
    case FIT_GLOBAL_SESSION:
        switch (enum_type)
        {
        case FIT_SESSION_SPORT_ENUM:
            switch (enum_value) {
                case FIT_SESSION_SPORT_ENUM_GENERIC:
                    strncpy(name, "Generic", size);
                    return 0;
                case FIT_SESSION_SPORT_ENUM_RUNNING:
                    strncpy(name, "Running", size);
                    return 0;
                case FIT_SESSION_SPORT_ENUM_CYCLING:
                    strncpy(name, "Cycling", size);
                    return 0;
                case FIT_SESSION_SPORT_ENUM_WALKING:
                    strncpy(name, "Walking", size);
                    return 0;
            }
        }
    }

    return 1;
}

double FIT_getRatio(int msg, int type)
{
    LOG_DEBUG("%s(%d, %d)", __FUNCTION__, msg, type);
    switch (msg) {
    case FIT_GLOBAL_RECORD:
        switch (type) {
        case FIT_RECORD_POSITION_LAT_S32:
        case FIT_RECORD_POSITION_LONG_S32:
            /* 180 / 1<<31 */
            return 8.381903171539306640625e-8;

        }
    }
    return 1;
}
