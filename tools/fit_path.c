#include <getopt.h>
#include <inttypes.h>
#include <libgen.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <png.h>

#include "libfit.h"
#include "libfit-messages.h"

#define die(...) do { fprintf(stderr, __VA_ARGS__); exit(1); } while(0)

#define DEFAULT_ZOOM 15
#define DEFAULT_OUTFILE "out.png"

struct params {
    char *png_file;
    char *fit_file;
    int zoom;
};

void usage(char *name)
{
    printf("usage: %s [--zoom zoom] [--out png_file] FIT\n",
           basename(name));
    printf("  -z, --zoom zoom: zoom to apply, default %d\n", DEFAULT_ZOOM);
    printf("  -o, --out png_file: file to generate, default %s\n",
           DEFAULT_OUTFILE);
}

void parse(int argc, char *argv[], struct params *par)
{
    while (1) {
        static struct option long_options[] = {
            {"zoom", required_argument, 0, 'z'},
            {"out", required_argument, 0, 'o'},
            {"help", required_argument, 0, 'h'},
            {0, 0, 0, 0}
        };

        int c = getopt_long(argc, argv, "z:o:h", long_options, NULL);
        if (c == -1) {
            if (!argv[optind])
                die("no file to process\n");

            else
                par->fit_file = strdup(argv[optind]);

            break;
        }
        switch (c) {

        case 'z':
            par->zoom = strtol(optarg, NULL, 0);
            break;

        case 'o':
            par->png_file = strdup(optarg);
            break;

        case 'h':
            usage(*argv);
            exit(0);
            break;

        default:
            die("bad param: %s\n", optarg);
        }
    }
}

struct point {
    /* position, read in degree then change to pixel  */
    double pos[2];
    /* speed in km/h */
    double speed;
    struct point *next;
};


struct point *extract_points(const char *file, struct point *points)
{
    /* openfile */
    struct point *ret = points, *last = points;

    fit_file_t *f;
    fit_record_t *rec = NULL;
    f = FIT_open(file);

    if (NULL == f)
        die("cannot dump %s\n", file);

    while (1) {
        /* read next record */
        rec = FIT_readRecord(f, rec);
        if (!rec)
            break;

        if (FIT_GLOBAL_RECORD != FIT_getRecordType(rec))
            continue;

        double x, y, s;

        if (3 == FIT_decodeValues(rec, FIT_GLOBAL_RECORD,
                FIT_RECORD_POSITION_LONG_S32, &x,
                FIT_RECORD_POSITION_LAT_S32, &y,
                FIT_RECORD_SPEED_U16, &s, -1)) {
            struct point *pt = calloc(1, sizeof *pt);
            if (NULL == pt)
                die("calloc failed");

            pt->pos[0] = x;
            pt->pos[1] = y;
            pt->speed = s;
            if (NULL == ret)
                ret = pt;

            else
                last->next = pt;

            last = pt;
        }
    }
    FIT_close(f);
    return ret;
}

void getbox(struct point *pts, double box[2][2])
{
    box[0][0] = box[0][1] = 180; 
    box[1][0] = box[1][1] = -180;
    while (pts)
    {
        if (pts->pos[0] < box[0][0]) box[0][0] = pts->pos[0];
        if (pts->pos[0] > box[1][0]) box[1][0] = pts->pos[0];
        
        if (pts->pos[1] < box[0][1]) box[0][1] = pts->pos[1];
        if (pts->pos[1] > box[1][1]) box[1][1] = pts->pos[1];
        
        pts = pts->next;
    } 
}

/**
 * convert longitude to pixel position in world map
 * longitude :]-180, 180]
 */
double long2x(double longitude, double zoom, double decal)
{
    double x;
    x = (longitude + 180.0) / 360. * pow(2.0, zoom);
    x += .5;
    x -= decal;
    x *= 256.0;
    x -= 128.0;
    return x;
}

/**
 * convert longitude to a tile index
 * 
 */
int long2xtile(double longitude, double zoom)
{
    double x;
    x = (longitude + 180.0) / 360. * pow(2.0, zoom);
    //x += 0.5;
    return (int)x;

}

double lat2y(double latitude, double zoom, double decal)
{
    double y;
    double tan_x = sin(latitude * M_PI / 180.0) / cos(latitude * M_PI / 180.0);
    y = (1. - log(tan_x + 1. / cos(latitude * M_PI / 180.)) / M_PI) / 2. * pow(2., zoom);
    y += 0.5;
    y -= decal;
    y *= 256.;
    y -= 128.;
    return y;
}

int lat2ytile(double latitude, double zoom)
{
    double y;
    double tan_x = sin(latitude * M_PI / 180.0) / cos(latitude * M_PI / 180.0);
    y = (1. - log(tan_x + 1. / cos(latitude * M_PI / 180.)) / M_PI) / 2. * pow(2., zoom);
    //y += 0.5;
    return (int)y;
}


void rasterize(struct point *pts, unsigned char **image, size_t w, size_t h, 
        int z, double decal[2])
{
    printf("%s: (%zux%zu)\n", __FUNCTION__, w, h);
    (*image) = calloc( w * h * 3, 1);
    printf("%p\n", *image);
    while (pts) {
        size_t x, y;
        x = long2x(pts->pos[0], z, decal[0]);
        y = lat2y(pts->pos[1], z, decal[1]);

        if (x>=w) x = w-1;

        if (y>= h) y = h-1;

        (*image)[3*(y*w + x)] = 255;

        pts = pts->next;
    }

}

void write_png_file(const char *file, unsigned char *image, size_t w,
                   size_t h)
{
    png_structp png;
    png_infop info;

    /* create file */
    FILE *f = fopen(file, "wb");
    if (!f)
    {
        perror(file);
        exit(1);
    }

    /* initialize stuff */
    png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png)
        die("png_create_write_struct failed");

    info = png_create_info_struct(png);
    if (!info)
        die("png_create_info_struct failed");

    png_init_io(png, f);

    png_set_IHDR(png, info, w, h,
                 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    png_write_info(png, info);

    w *= 3;
    while (h--) {
        png_write_row(png, image);
        image += w;
    }
    png_write_end(png, NULL);

    fclose(f);
}

int main(int argc, char *argv[])
{
    struct params params = { NULL, NULL, DEFAULT_ZOOM };

    parse(argc, argv, &params);

    struct point *pts;
    double decal[2], box[2][2];


    pts = extract_points(params.fit_file, NULL);

    getbox(pts, box);

    puts("Bounding box in degree");
    printf("%g --> %g\n", box[0][0], box[1][0]);
    printf("%g --> %g\n", box[0][1], box[1][1]);
    
    printf("x: %d --> %d\n", 
            long2xtile(box[0][0], params.zoom), 
            long2xtile(box[1][0], params.zoom)); 

    printf("y: %d --> %d\n", 
            lat2ytile(box[1][1], params.zoom), 
            lat2ytile(box[0][1], params.zoom)); 

    /* remember that latitude pixel go from north to south */

    decal[0] = long2xtile(box[0][0], params.zoom);
    decal[1] = lat2ytile(box[1][1], params.zoom);
    // get top left tile in osm coordinates
    puts("\nBounding box in pixels");
    printf("%g --> %g\n", long2x(box[0][0], params.zoom, decal[0]), 
            long2x(box[1][0], params.zoom, decal[0]));
    printf("%g --> %g\n", lat2y(box[1][1], params.zoom, decal[1]), 
            lat2y(box[0][1], params.zoom, decal[1]));

        unsigned char *image;
    size_t w, h;
    w = 256*(long2xtile(box[1][0], params.zoom) -  
            long2xtile(box[0][0], params.zoom) + 1 );

    h = 256*(lat2ytile(box[0][1], params.zoom)- 
            lat2ytile(box[1][1], params.zoom) + 1);
    rasterize(pts, &image, w, h, params.zoom, decal); 


    write_png_file("test.png", image, w, h);

    return 0;
}
