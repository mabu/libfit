#include <dirent.h>
#include <errno.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/stat.h>
#include <sys/types.h>
/*  usage: process <files to process dir> <backup dir> <www dir> */

int main(int argc, char *argv[])
{
    int ret;
    char *in = NULL;
    char *out = NULL;
    char *www = NULL;
	char cmd[1024], from[1024], to[1024];
    switch (argc) {
    case 4:
            in = argv[1];
            out = argv[2];
            www = argv[3];
            break;
    case 3:
            in = argv[1];
            www = argv[2];
            break;
    default:
        printf("usage: %s <files to process dir> [<backup dir>] <www dir>\n", basename(*argv));
        return 1;
    }

    if (out) {
        ret = mkdir(out, 0755);
        if (-1 == ret && errno != EEXIST) {
            perror(out);
            return 2;
        }
    }
    ret = mkdir(www, 0755);
    if (-1 == ret && errno != EEXIST) {
        perror(www);
        return 3;
    }

    sprintf(to, "%s/maps", www);
    ret = mkdir(to, 0755);
    if (-1 == ret && errno != EEXIST) {
        perror(www);
        return 4;
    }

    struct stat statbuf;

    ret = stat(to, & statbuf);
    if (0 != ret) {
        perror(to);
        return 5;
    }
    if ((statbuf.st_mode & S_IFMT) != S_IFDIR) {
        fprintf(stderr, "Error: can't create directory '%s'\n", to );
        return 6;
    }


	DIR * dir = opendir(in);
	if (NULL == dir) {
		perror(in);
		return 4;
	}

	struct dirent *entry;
	
	while(entry=readdir(dir)) {
		char *nm = entry->d_name;
		int len = strlen(nm);
		if ((DT_REG != entry->d_type) ||
			('.' == nm[0]) ||
			(len < 5) ||
			(strcasecmp(".FIT", nm + len - 4))) {
				continue;
			}
        sprintf(cmd, "fit2json --out \"%s/maps\" \"%s/%s\"", www, in, entry->d_name);
        // printf("cmd: '%s'\n", cmd);
        ret = system(cmd);
        if (0 != ret) {
		    fprintf(stderr, "FAILED: %s\n", cmd);
		    //fprintf(stderr, "FAILED: fit2json --out \"%s/maps\" \"%s/%s\"", www, in, entry->d_name);
            continue;
        }

        if (out) {
    		sprintf(from, "%s/%s", in, entry->d_name);
    		sprintf(to, "%s/%s", out, entry->d_name);
            ret = rename(from, to);

            if (0 != ret) {
                perror("rename");
            }
        }
	}
	closedir(dir);
	return 0;
}
