#include <stdio.h>
#include <libgen.h>
#include <limits.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libfit.h"
#include "libfit-messages.h"

typedef struct
{
    int first_file;
    int display_header;
} param_t;

/* possible options:
     * --headers
     * --fields <fields> --
 */

void usage(char *name)
{
    printf("usage: %s [--headers] FILE ... \n", basename(name));
}

int decode_params(int argc, char *argv[], param_t *params)
{
    int i;
    params->first_file = 1;
    params->display_header = 0;
    for (i = 1; i < argc; ++i)
    {
        if (0 == strcmp("--headers", argv[i]))
        {
            params->display_header = 1;
            params->first_file = i+1;
        }
    }
    return 0;
}

void display_headers()
{
    printf("name\tdate\tdistance(m)\ttotal time\t");
    printf("elapsed time\tpause\t");
    printf("pace\tstddev(%%)\t");
    puts("");
}

void format_time(int time_sec)
{
    int hour = time_sec / 3600;
    int min = (time_sec / 60)%60;
    int sec = time_sec % 60;
    printf("%d:%02d:%02d\t", hour, min, sec);
}

#define SECONDS_PER_DAY 86400
#define DAYS_PER_4YEARS 1461
#define LEAP_DAY_IDX 790

void format_date(uint32_t time_sec)
{
    //time_t date = time_sec;
    //struct tm *tm = gmtime(&date);

    //printf("%04d/%02d/%02d\t", 1900 + tm->tm_year, tm->tm_mon, tm->tm_mday);

    int year, month, day;
    int months[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    day = time_sec / SECONDS_PER_DAY;
    year = 4*(day / DAYS_PER_4YEARS) + 1990;

    day = day % DAYS_PER_4YEARS;
    if (LEAP_DAY_IDX == day)
    {
        month = 2;
        day = 29;
        year += 2;
        goto END;
    }
    else if (day > LEAP_DAY_IDX  )
    {
        -- day;
    }
    for (month = 0; ; ++month, month %= 12)
    {
        if (day > months[month])
            day -= months[month];
        else
            break;

        if (11 == month)
            year++;
        
    }
    ++month;
    

END:
    printf("%04d/%02d/%02d\t", year, month, day);
}


void format_pace(int pace_sec)
{
    int min = (pace_sec / 60);
    int sec = pace_sec % 60;
    printf("%2d:%02d\t", min, sec);
}

void process_files(char *files[])
{
    while (*files) {
        uint32_t timestamp = 0xFFFFFFFF;
        double total_distance = -1, total_time = 0, elapsed_time = 0;
        double speed_sum = 0, speed_sq_sum = 0;
        int speed_cnt = 0;
	    fit_record_t *rec = NULL;
	    fit_file_t *f;
	    f = FIT_open(*files);

        if (NULL == f)
        {
            continue;
        }

	    while (1)
        {
	        rec = FIT_readRecord(f, rec);
	        if (NULL == rec) {
		        break;
	        }
	        switch (FIT_getRecordType(rec)) {
                case FIT_GLOBAL_FILE_ID:
                {
                    FIT_getValues(rec, FIT_GLOBAL_FILE_ID,
                        FIT_FILE_TIME_STAMP, &timestamp, -1);
                    break;
                }
	            case FIT_GLOBAL_RECORD:
		        {
		            double speed;
		            FIT_decodeValues(rec, FIT_GLOBAL_RECORD,
				        FIT_RECORD_SPEED_U16, &speed, -1);
                    ++speed_cnt;

                    speed_sum += speed;
                    speed_sq_sum += speed*speed;

		            break;
		        }
	            case FIT_GLOBAL_SESSION:
                {
                    uint32_t total;
                    FIT_getValues(rec, FIT_GLOBAL_SESSION,
                        FIT_SESSION_TOTAL_DISTANCE_U32, &total, -1);
                    if (total == 0 || total == 0xFFFFFFFF)
                        break;
                    FIT_decodeValues(rec, FIT_GLOBAL_SESSION,
                        FIT_SESSION_TOTAL_ELAPSED_TIME_U32, &elapsed_time,
                        FIT_SESSION_TOTAL_TIMER_TIME_U32, &total_time,
                        FIT_SESSION_TOTAL_DISTANCE_U32, &total_distance, -1);
                }
            }
        }

        if (total_distance < 0)
        {
            fprintf(stderr, "Error: total_distance not valid is %s\n",
                    basename(*files));
            goto NEXT;
        }
        /* file name */
        printf("%s\t", basename(*files));
        /* date */
        if (0xFFFFFFFF != timestamp)
        {
            format_date(timestamp);        
        }
        else
        {
            printf("\t");
        }
        /*distance*/
        total_distance *= 10;
        printf("%d\t", (int)total_distance);
        /*total time*/
        format_time(total_time);
        /*elapsed time*/
        format_time(elapsed_time);
        /*pause*/
        format_time(elapsed_time - total_time);
        /*pace*/
        format_pace((int)1000.*total_time / total_distance);
        //format_time((int)(pace_sum / (double)speed_cnt));
        /*stddev*/
        {
            float mean = speed_sum / speed_cnt;
            float stddev = sqrtf(speed_sq_sum/speed_cnt - speed_sum/speed_cnt*speed_sum/speed_cnt);
            printf("%2.2f\t", 100.f * stddev / mean);
        }
        puts("");

NEXT:
        //printf("total time:\t%f\n", total_time);
        //printf("total distance:\t%f\n", total_distance);
	    FIT_close(f);
        files++;
	}
}



int main(int argc, char *argv[])
{
    param_t params = {0};

    if (1 == argc)
    {
        usage (*argv);
        return 0;
    }

    decode_params(argc, argv, &params);

    if (params.display_header)
    {
        display_headers();
    }

    process_files(argv + params.first_file);

    return 0;
}
