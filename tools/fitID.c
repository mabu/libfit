#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "libfit.h"
#include "libfit-messages.h"

#define die(...) do { fprintf(stderr, __VA_ARGS__); exit(1); } while(0)

#define DEFAULT_OUTFILE "out.json"

struct params {
    char *json_file;
    char *fit_file;
};

void usage(char *name)
{
    printf("usage: %s [-h] FIT\n", basename(name));
    printf("  -h, --human   display human readable date");
}

uint32_t get_timestamp(const char *file)
{
    fit_file_t *f;
    fit_record_t *rec = NULL;
    f = FIT_open(file);

    uint32_t timestamp = 0;

    struct tm fit_epoch = {0, 0, 0, 31, 11, 89, 0, 0, 0, 0, NULL};
    uint32_t FIT_EPOCH = mktime(&fit_epoch);

    if (NULL == f)
        die("cannot dump %s\n", file);

    while (1) {
        /* read next record */
        fit_record_t *rc = FIT_readRecord(f, rec);
        if (!rc) {
            FIT_free(rec);
            break;
        }
        rec = rc;

        /* the first record: read timestamp*/
        if (FIT_GLOBAL_FILE_ID == FIT_getRecordType(rec)) {
            if (1 == FIT_getValues(rec, FIT_GLOBAL_FILE_ID, FIT_FILE_TIME_STAMP, &timestamp, -1)) {
                break;
            }
        }
    }
    FIT_free(rec);
    FIT_close(f);
#ifndef NDEBUG
    fprintf(stderr, "Timestamp: %lu\n", timestamp );
    fprintf(stderr, "FIT EPOCH: %lu\n", FIT_EPOCH );
    fprintf(stderr, "Timestamp: %lu\n", timestamp+FIT_EPOCH );
#endif 
    return timestamp+FIT_EPOCH;

}

const char * sprint_timestamp(char *sz, uint32_t timestamp)
{
    // date is in form yyyy-dd-mmThh:mm:ss : 20
    if (NULL == sz) {
        sz = calloc(1, 20);
    }

    time_t ts = timestamp;
    struct tm res;
    localtime_r(&ts, &res);
    sprintf(sz, "%4d-%02d-%02dT%02d:%02d:%02d",
            res.tm_year+1900, res.tm_mon+1, res.tm_mday,
            res.tm_hour, res.tm_min, res.tm_sec);

    return sz;
}

const char *sprint_ID(char *ID, uint32_t timestamp)
{
    if (NULL == ID) {
        ID = calloc(1, 9);
    }

    char base36[36];

    for (int i = 0, c = '0'; i < 10; ++i, ++c) {
        base36[i] = c;
    }

    for (int i = 10, c = 'A'; i < 36; ++i, ++c) {
        base36[i] = c;
    }

    time_t ts = timestamp;
    struct tm res;
    localtime_r(&ts, &res);
    /*sprintf(timestamp, "%4d-%02d-%02dT%02d:%02d:%02d",
            res.tm_year+1920, res.tm_mon+1, res.tm_mday-1,
            res.tm_hour, res.tm_min, res.tm_sec);
    */
    int bs[4];
    bs[0] = res.tm_year-110;
    bs[1] = res.tm_mon +1;
    bs[2] = res.tm_mday;
    bs[3] = res.tm_hour+1;
    for (int i = 0; i < 4; ++i) {
        if (bs[i] >=36 || bs[i] < 0) {
            fprintf(stderr, "Error decoding timestamp %4d-%02d-%02dT%02d:%02d:%02d\n",
            res.tm_year+1900, res.tm_mon+1, res.tm_mday,
            res.tm_hour, res.tm_min, res.tm_sec);
            goto EXIT;
        }
    }

    sprintf(ID, "%c%c%c%c%02d%02d",
            base36[bs[0]], base36[bs[1]],
            base36[bs[2]], base36[bs[3]],
            res.tm_min, res.tm_sec);
EXIT:
    return ID;
}

int main(int argc, char *argv[])
{

    if (argc != 2 && argc != 3) {
        usage(*argv);
        exit(0);
    }

    if (3 == argc) {
        if (strcmp("-h", argv[1]) && strcmp("--human", argv[1]))
        {
            usage(*argv);
            exit(0);
        }
        char ts[20] = "";
        if (sprint_timestamp(ts, get_timestamp(argv[2])))
        {
            printf(ts);
        }

    } else {

        char ID[8] = "";
        //if (extract_ID(argv[1], ID))
        if (sprint_ID(ID, get_timestamp(argv[1])))
        {
            printf(ID);
        }
    }

    return 0;
}
