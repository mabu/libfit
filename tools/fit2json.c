#include <getopt.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <libgen.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <json-c/json.h>
#include <math.h>

#include "libfit.h"
#include "libfit-messages.h"

#define die(...) do { fprintf(stderr, __VA_ARGS__); exit(1); } while(0)

#define DEFAULT_OUTFILE "out.json"

struct params {
    char *out;
    char *fit_file;
};

void usage(char *name)
{
    printf("usage: %s [--out json_file] FIT\n", basename(name));
    printf("       %s [--help]\n", basename(name));
    printf("       %s [--version]\n", basename(name));
    printf("  -o, --out json_file: file to generate, default %s\n",
           DEFAULT_OUTFILE);
}

void handle_out_param(struct params *par) {
    struct stat st;
    if (0 == stat(par->out, &st) && (st.st_mode & S_IFDIR)) {
        /* 'out' is a directory, redirect to "out/fit.json" */
        char *incp, *p;
        p = strrchr(par->fit_file, '/');
        if (p) {
            incp = strdup(p+1);
        } else {
            incp = strdup(par->fit_file);
        }
        char *buff = malloc(strlen(par->out) + strlen(par->fit_file) + 5);
        if (NULL == buff || NULL == incp) {
            perror("malloc");
            die("\n");
        }

        p = strrchr(incp, '.');
        if (p) {
            *p = '\0';
        }

        /* fprintf(stderr, "redirecct to %s .. %s .. %s\n", out, incp, in); */

        sprintf(buff, "%s/%s.json", par->out, incp);

        free(par->out);
        par->out = buff;
    }

}

void parse(int argc, char *argv[], struct params *par)
{
    while (1) {
        static struct option long_options[] = {
            {"out", required_argument, 0, 'o'},
            {"help", no_argument, 0, 'h'},
            {"version", no_argument, 0, 'v'},
            {0, 0, 0, 0}
        };

        int c = getopt_long(argc, argv, "o:hv", long_options, NULL);
        if (c == -1) {
            if (!argv[optind]){
                die("no file to process\n");
            }
            else{
                /* assert optind is the last parameter */
                if (optind != (argc-1)) {
                    die("Only one file to process");
                }
                par->fit_file = strdup(argv[optind]);
            }
            break;
        }
        switch (c) {

        case 'o':
            par->out = strdup(optarg);
            break;

        case 'v':
            puts(FIT_info());
            break;

        case 'h':
            usage(*argv);
            exit(0);
            break;

        default:
            die("bad param: %s\n", optarg);
        }
    }
    if (NULL == par->out) {
        par->out = strdup(DEFAULT_OUTFILE);
    } else {
        handle_out_param(par);
    }
}

struct point {
    /* position, read in degree then change to pixel  */
    double pos[2];
    /* speed in km/h */
    double speed;
    double distance;
    double altitude;
    double heart_rate;
    uint32_t ts;
    struct point *next;
};


struct point *extract_data(const char *file, struct point *points, char *timestamp)
{
    /* openfile */
    struct point *ret = points, *last = points;

    fit_file_t *f;
    fit_record_t *rec = NULL;
    f = FIT_open(file);

    uint32_t start_timestamp = 0;

    if (NULL == f)
        die("cannot dump %s\n", file);

    while (1) {
        /* read next record */
        fit_record_t *rc = FIT_readRecord(f, rec);
        if (!rc) {
            FIT_free(rec);
            break;
        }
        rec = rc;

        /* the first record: read timestamp*/
        if (FIT_GLOBAL_FILE_ID == FIT_getRecordType(rec)) {
            if (1 == FIT_getValues(rec, FIT_GLOBAL_FILE_ID, FIT_FILE_TIME_STAMP, &start_timestamp, -1)) {
                if (timestamp) {
                    time_t ts = start_timestamp;
                    struct tm res;
                    localtime_r(&ts, &res);
                    /* @TODO correct this (not important: only send to stderr today)*/
                    sprintf(timestamp, "%4d-%02d-%02dT%02d:%02d:%02d",
                            res.tm_year+1920, res.tm_mon+1, res.tm_mday-1,
                            res.tm_hour, res.tm_min, res.tm_sec);
                }
            }
            continue;
        }

        /* if record is not a record, skip it */
        if (FIT_GLOBAL_RECORD != FIT_getRecordType(rec))
            continue;

        double x=-1, y=-1, s=-1, d=-1, r=-1, a=-1;
        int32_t ts = 0;
        int good;
        /* get timestamp */
        if (1 == FIT_getValues(rec, FIT_GLOBAL_RECORD, FIT_RECORD_TIMESTAMP_U32, &ts, -1)) {
            ts -= start_timestamp;
        }

        /* decode position and speed */
        good =  FIT_decodeValues(rec, FIT_GLOBAL_RECORD,
                FIT_RECORD_DISTANCE_U32, &d,
                FIT_RECORD_SPEED_U16, &s,
                FIT_RECORD_POSITION_LONG_S32, &x,
                FIT_RECORD_POSITION_LAT_S32, &y,
                FIT_RECORD_ALTITUDE_U16, &a,
                FIT_RECORD_HEART_RATE_U8, &r, -1);

        if (good > 0)
        {
            struct point *pt = calloc(1, sizeof *pt);
            if (NULL == pt)
                die("calloc failed");

            pt->pos[0] = x;
            pt->pos[1] = y;
            pt->speed = s;
            pt->distance = d;
            pt->altitude = a;
            pt->heart_rate = r;
            pt->ts = ts;
            if (NULL == ret)
                ret = pt;

            else
                last->next = pt;

            last = pt;
        }
    }
    FIT_close(f);
    return ret;
}

void free_data(struct point *pts)
{
    struct point *next;
    while(pts) {
        next = pts->next;
        free(pts);
        pts = next;
    }
}

void extract_coordinates(json_object *obj, struct point *pts, struct point *mean)
{
    double mean_pos[2] = {};
    double mean_sp = 0;
    double dist = 0;
    uint32_t mtime = 0;
    int mean_n = 0;

    while(pts) {
        if ((isnan(pts->pos[0])) || (isnan(pts->pos[1])))
        {
            pts = pts->next;
            continue;
        }
        mean_pos[0] += pts->pos[0];
        mean_pos[1] += pts->pos[1];
        mean_sp += pts->speed;
        ++mean_n;

        struct json_object * xy = json_object_new_array();
        json_object_array_add(xy, json_object_new_double(pts->pos[0]));
        json_object_array_add(xy, json_object_new_double(pts->pos[1]));
        json_object_array_add(obj, xy);

        if (NULL == pts->next) {
            dist = pts->distance;
            mtime = pts->ts;
        }
        pts = pts->next;
    }

    if (mean) {
        mean->pos[0] = mean_pos[0] / mean_n;
        mean->pos[1] = mean_pos[1] / mean_n;
        mean->speed = mean_sp / mean_n;
        mean->distance = dist;
        mean->ts = mtime;
    }

}

void extract_time(json_object *obj, struct point *pts)
{
    while(pts) {
        if (0 == pts->ts)
        {
            pts = pts->next;
            continue;
        }
        json_object_array_add(obj, json_object_new_int(pts->ts));
        pts = pts->next;
    }
}
void extract_distance(json_object *obj, struct point *pts)
{
    while(pts) {
        if (isnan(pts->distance))
        {
            pts = pts->next;
            continue;
        }

        struct json_object * xy = json_object_new_array();
        json_object_array_add(xy, json_object_new_int(pts->ts));
        json_object_array_add(xy, json_object_new_double(pts->distance));
        json_object_array_add(obj, xy);
        pts = pts->next;
    }
}

void extract_speed(json_object *obj, struct point *pts)
{
    while(pts) {
        if (isnan(pts->speed))
        {
            pts = pts->next;
            continue;
        }
        struct json_object * xy = json_object_new_array();
        json_object_array_add(xy, json_object_new_int(pts->ts));
        json_object_array_add(xy, json_object_new_double(pts->speed));
        json_object_array_add(obj, xy);
        pts = pts->next;
    }
}

void extract_altitude(json_object *obj, struct point *pts)
{
    while(pts) {
        if (isnan(pts->altitude))
        {
            pts = pts->next;
            continue;
        }
        struct json_object * xy = json_object_new_array();
        json_object_array_add(xy, json_object_new_int(pts->ts));
        json_object_array_add(xy, json_object_new_double(pts->altitude));
        json_object_array_add(obj, xy);
        pts = pts->next;
    }
}

void extract_heart_rate(json_object *obj, struct point *pts)
{
    while(pts) {
        if (isnan(pts->heart_rate))
        {
            pts = pts->next;
            continue;
        }
        struct json_object * xy = json_object_new_array();
        json_object_array_add(xy, json_object_new_int(pts->ts));
        json_object_array_add(xy, json_object_new_double(pts->heart_rate));
        json_object_array_add(obj, xy);
        pts = pts->next;
    }
}

#if 0
void redirect(const char *out, const char *in)
{
    if (NULL == out) {
        if (NULL == freopen(DEFAULT_OUTFILE, "w", stdout)) {
            perror(DEFAULT_OUTFILE);
            die("\n");
        }
    } else {
        struct stat st;
        if (0 == stat(out, &st) && (st.st_mode & S_IFDIR)) {
            /* 'out' is a directory, redirect to "out/fit.json" */
            char *incp, *p;
            p = strrchr(in, '/');
            if (p) {
                incp = strdup(p+1);
            } else {
                incp = strdup(in);
            }
            char *buff = malloc(strlen(out) + strlen(in) + 5);
            if (NULL == buff || NULL == incp) {
                perror("malloc");
                die("\n");
            }

            p = strrchr(incp, '.');
            if (p) {
                *p = '\0';
            }

            /* fprintf(stderr, "redirecct to %s .. %s .. %s\n", out, incp, in); */

            sprintf(buff, "%s/%s.json", out, incp);


            if (NULL == freopen(buff, "w", stdout)) {
                perror(buff);
                die("\n");
            }

            free(buff);
        } else {
            /* given file name, try to redict to it */
            if (NULL == freopen(out, "w", stdout)) {
                perror(out);
                die("\n");
            }
        }
    }
}
#endif

int main(int argc, char *argv[])
{
    struct params params = { NULL, NULL };

    parse(argc, argv, &params);

    struct point *pts, mean = {};

    char timestamp[20], date[11], time[9];
    pts = extract_data(params.fit_file, NULL, timestamp);

    // fprintf(stderr, "%s\n", timestamp);
    sscanf(timestamp, "%[^T]T%s", date, time);

    /* prepare the json file */
    struct json_object *obj, *path, *coordinates, *pos, *mean_pos;
    struct json_object *timeo, *distance, *speed, *altitude, *hr;
    obj = json_object_new_object();
    path = json_object_new_object();
    json_object_object_add(path, "type", json_object_new_string("LineString"));
    coordinates = json_object_new_array();

    /* path */
    extract_coordinates(coordinates, pts, &mean);

    /* finalize */
    json_object_object_add(path, "coordinates", coordinates);
    json_object_object_add(obj, "path", path);


    /* mean position */
    pos = json_object_new_object();
    json_object_object_add(pos, "type", json_object_new_string("Point"));
    mean_pos = json_object_new_array();
    json_object_array_add(mean_pos, json_object_new_double(mean.pos[0]));
    json_object_array_add(mean_pos, json_object_new_double(mean.pos[1]));
    json_object_object_add(pos, "coordinates", mean_pos);

    json_object_object_add(obj, "pos", pos);


    json_object_object_add(obj, "date", json_object_new_string(date));
    json_object_object_add(obj, "start_time", json_object_new_string(time));
    json_object_object_add(obj, "mspeed", json_object_new_double(mean.speed));
    json_object_object_add(obj, "total_distance", json_object_new_double(mean.distance));
    json_object_object_add(obj, "total_time", json_object_new_double(mean.ts));



    timeo = json_object_new_array();
    extract_time(timeo, pts);

    json_object_object_add(obj, "time", timeo);

    distance = json_object_new_array();
    extract_distance(distance, pts);
    json_object_object_add(obj, "distance", distance);

    speed = json_object_new_array();
    extract_speed(speed, pts);
    json_object_object_add(obj, "speed", speed);

    altitude = json_object_new_array();
    extract_altitude(altitude, pts);
    json_object_object_add(obj, "altitude", altitude);

    hr = json_object_new_array();
    extract_heart_rate(hr, pts);
    json_object_object_add(obj, "heart_rate", hr);




    /* save json file */
    json_object_to_file(params.out, obj);


    /* clean up */
    free_data(pts);
    free(params.fit_file);
    free(params.out);


    return 0;
}
