#include <math.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <string.h>
#include <getopt.h>
#include <dirent.h>
#include <limits.h>
#include <json-c/json.h>
#include <sys/stat.h>
#include "../config.h"
typedef struct options {
    int keep;
    int json;
    int sql;
    int force;
    float minspd;
    float maxspd;
    char *out;
    char *www;
} opts_t;

#define DEFAULT_OPTS {0, 1, 0, 0, 0, FLT_MAX, "base", "www"}


void usage(char *name)
{
    opts_t deflt = DEFAULT_OPTS;
    printf("usage:\n");
    printf("  %s [--help]\n", name);
    printf("  %s [options]\n", name);
    printf(" options are: \n");
    printf("      --out <outfile>: json file to be generated in www directory (default: %s.json)\n", deflt.out);
    printf("      --www <directory>: directory containing maps/*.json files (the run files), default: %s\n", deflt.www);
    printf("      --force: erase outfile is existing\n");
    printf("      --sql: generate sqlite base instead of json\n");
    printf("      --keep: keep file that generated the sqlite base\n");
    printf("      --minspd <speed>: files which mean speed under this value are filtered out");
    printf("      --maxspd <speed>: files which mean speed over this value are filtered out");
    printf("      --help: display this help\n");
}

int decode_opt(int argc, char *argv[], opts_t *opts)
{
    int ret;
    while (1) {
        int idx;
        static struct option opt[] = {
            { "out", required_argument, 0, 0},
            { "www", required_argument, 0, 0},
            { "force", no_argument, 0, 0},
            { "keep", no_argument, 0, 0},
            { "sql", no_argument, 0, 0},
            { "minspd", required_argument, 0, 0},
            { "maxspd", required_argument, 0, 0},
            { "help", no_argument, 0, 0},
            { 0, 0, 0, 0}
        };
        ret = getopt_long(argc, argv, "", opt, &idx);
        if (-1 == ret)
            break;

        if ('?' == ret) {
            return -1;
        }

        switch (idx) {
            case 0:
                // --out
                opts->out = optarg;
                break;

            case 1:
                // --www
                opts->www = optarg;
                break;

            case 2:
                // --force
                opts->force = 1;
                break;

            case 3:
                // --keep
                opts->keep = 1;
                break;

            case 4:
                // --sql
                opts->sql = 1;
                opts->json = 0;
                break;

            case 5:
                // --minspd
                opts->minspd = strtof( optarg, NULL);
                break;

            case 6:
                // --maxspd
                opts->maxspd = strtof( optarg, NULL);
                break;

            default:
                    return -1;

        }
    }
    return 0;
}


char * manage_output_name(opts_t opts)
{
    /* build output file path */
    char *out = malloc(7+strlen(opts.out)+strlen(opts.www));
    if (NULL == out) {
        perror("malloc");
        return NULL;
    }
    strcpy(out, opts.www);
    strcat(out, "/");
    strncat(out, opts.out, strrchr(opts.out, '.') - opts.out);
    if (opts.sql) {
        strcat(out, ".sql");
    } else {
        strcat(out, ".json");
    }

    /* check if file already exist */
    struct stat statbuf;
    if (0 == stat(out, & statbuf)) {
        if (opts.force) {
            remove(out);
        } else {
            fprintf(stderr, "Error: '%s' not empty. Use '--force' option\n", out);
            free(out);
            out = NULL;
        }
    }
    return out;
}

int check_system(void)
{
    /* check jq is installed */
    FILE * f = popen("jq --version", "r");
    if (NULL == f) {
        perror ("popen");
        return 1;
    }

    while(EOF != getc(f));

    if (0 != pclose(f)) {
        fprintf (stderr, "Error executing jq\n");
        return 1;
    }
    return 0;
}

int get_exe_dir(char *buff) {
    if (NULL == realpath("/proc/self/exe", buff))
        return 1;
    char * p = strrchr(buff, '/');
    printf("buff: '%s'\n", buff);
    if (p != buff) {
        *p ='\0';
    }
    strcat(buff, "/../share/" PACKAGE);
    printf("buff: '%s'\n", buff);
    return 0;
}

int copy_file(const char *name, const char *from, const char *to)
{
    printf("Copy %s %s %s\n", name, from, to);
    char path[PATH_MAX];
    FILE *f, *fout;
    sprintf(path, "%s/%s", from, name);
    f = fopen(path, "rb");

    sprintf(path, "%s/%s", to, name);
    fout = fopen(path, "wb");

    // TODO check f and fout here

    char mem[256];
    while(fgets(mem, sizeof mem, f)) {
        fputs(mem, fout);
    }
    fclose(f);
    fclose(fout);
    return 0;
}

int copy_files(const char *dest) {
    FILE *f, *fout;
    char buff[PATH_MAX];
    char mem[256];
    char path[PATH_MAX+20];
    if ( 0!= get_exe_dir(buff)) {
        fprintf(stderr, "Error: can't get exe dir\n");
        return EXIT_FAILURE;
    }

    // read apikey
    char *apikey = NULL;
    sprintf(path, "%s/apikey", buff);
    f = fopen(path, "rb");
    if (f) {
        fseek(f, 0, SEEK_END);
        size_t len = ftell(f);
        rewind(f);
        apikey = malloc(len+1);
        fgets(apikey, len+1, f);
        apikey[strcspn(apikey, "\n\r")] = '\0';
        fclose(f);
    } else {
        printf("apikey not found at %s\n", path);
    }

    if (NULL == apikey) {
        apikey = "";
    }

    printf("apikey: '%s'\n", apikey);

    // copy data-loader.js with a modification
    sprintf(path, "%s/data-loader.js", buff);
    printf("opening %s\n", buff);
    f = fopen(path, "rb");
    sprintf(path, "%s/data-loader.js", dest);
    fout = fopen(path, "wb");

    // TODO check f and fout here
    while(fgets(mem, sizeof mem, f)) {
        char * p = strstr(mem, "@APIKEY@");
        if (p) {
            *p = '\0';
            fputs(mem, fout);
            fputs(apikey, fout);
            fputs(p+8, fout);
        } else {
            fputs(mem, fout);
        }
    }
    fclose(f);
    fclose(fout);

    copy_file("styles.css", buff, dest);
    copy_file("index.html", buff, dest);

    return 0;
}

int main(int argc, char *argv[])
{
    opts_t opts = DEFAULT_OPTS;
    int kept = 0, ignored = 0;
    /* decode paramters */
    if (decode_opt(argc, argv, &opts)) {
        usage(basename(*argv));
        return EXIT_FAILURE;
    }

    /* get ouput name */
    char *out =  manage_output_name(opts);
    if (NULL == out) {
        return EXIT_FAILURE;
    }

    /* case of json base */
    size_t szDir = 17+strlen(opts.www);
    char *dirpath = malloc(szDir);
    char *path = NULL;
    sprintf(dirpath, "%s/maps/", opts.www);
    DIR *dir = opendir(dirpath);
    struct dirent * de;
    double mspeed;

    struct json_object *base;
    base = json_object_new_array();

    while (1) {
        const char *ext;
        de = readdir(dir);

        if (NULL == de)
            break;

        /* process only regular files */
        if (DT_REG != de->d_type)
            continue;

        /* check json extension */
        ext = strrchr(de->d_name, '.');
        if (ext && 0 == strcmp(ext, ".json")) {

            path = realloc(path, szDir + strlen(de->d_name));
            sprintf(path, "%s%s", dirpath, de->d_name);

            json_object *jo = json_object_from_file(path);
            json_object *data = json_object_new_object();
            json_object *date, *ID, *dist, *time, *speed;

            ID = json_object_new_string(de->d_name);
            date = json_object_object_get(jo, "date");
            speed = json_object_object_get(jo, "mspeed");
            dist = json_object_object_get(jo, "total_distance");
            time = json_object_object_get(jo, "total_time");

            mspeed = json_object_get_double(speed);
            if (mspeed < opts.minspd || mspeed > opts.maxspd || !isnormal(mspeed)) {
                printf("speed: %lf, ignoring\n", mspeed);
                ++ignored;
                json_object_put(ID);
                json_object_put(date);
                json_object_put(speed);
                json_object_put(dist);
                json_object_put(time);
                json_object_put(data);
                continue;
            }
            printf("speed: %lf, keeping\n", mspeed);
            ++kept;

            /* TODO  assert no one is NULL */

            json_object_object_add(data, "ID", ID);
            json_object_object_add(data, "date", date);
            json_object_object_add(data, "total_distance", dist);
            json_object_object_add(data, "total_time", time);
            json_object_object_add(data, "mspeed", speed);

            json_object_array_add(base, data);

        }
    }
    json_object_to_file(out, base);
    json_object_put(base);

    printf("%d files ignored, %d kept\n", ignored, kept);

    copy_files(opts.www);

    /* clean up */
    closedir(dir);
    free(out);
    free(path);
    free(dirpath);
    return EXIT_SUCCESS;
}
