#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include "libfit.h"
#include "libfit-messages.h"

#define die(...) do { fprintf(stderr, __VA_ARGS__); exit(1); } while(0)

void format_time(int time_sec)
{
    int hour = time_sec / 3600;
    int min = (time_sec / 60) % 60;
    int sec = time_sec % 60;
    printf("%d:%02d:%02d\t", hour, min, sec);
}

void dump_file(const char *file)
{
    int cnt = 0;
    fit_file_t *f;
    fit_record_t *rec = NULL;
    f = FIT_open(file);

    if (NULL == f) {
        die("cannot dump %s\n", file);
    }

    while (1) {
        char name[64];
        ++cnt;
        /* read next record */
        rec = FIT_readRecord(f, rec);
        if (NULL == rec) {
            break;
        }

        /* get its type */
        int rectype = FIT_getRecordType(rec);
        if (FIT_decodeRecordType(name, sizeof name, rectype))
            printf("%3d: Type: ??? (%d)\n", cnt, rectype);
        else
            printf("%3d: Type: %s (%d)\n", cnt, name, rectype);

        /* get its fields */
        fit_def_t *def = FIT_getDefinition(rec);
        int fcnt = FIT_getFieldsCount(def);
        for (int f = 0; f < fcnt; ++f)
        {
            struct fit_field_info ffi;
            const char *type = FIT_getFieldType(def, f);
            const char *name = FIT_getFieldName(def, f);
            FIT_getFieldInfo(def, f, &ffi);
            printf("    %d: %s %s %d byte%s ", f,
                    name?name:"NULL", type?type:"NULL",
                    ffi.size, ffi.size>1?"s":" ");
            switch (ffi.type)
            {
                case FIT_TYPE_int8:
                    printf("(%hhd)", *(int8_t*)FIT_getValue(rec, f));
                    break;
                case FIT_TYPE_uint8:
                    printf("(%hhu)", *(uint8_t*)FIT_getValue(rec, f));
                    break;
                case FIT_TYPE_enum:
                    {
                        uint8_t value = *(uint8_t*) FIT_getValue(rec, f);
                        /* try to decode enum as string */
                        char buffer[64];
                        if (FIT_decodeEnum(buffer, sizeof buffer, rectype, ffi.name, value)) {
                            printf("(%hhu)", value);
                        } else {
                            printf("(%s)", buffer);
                        }

                    }
                    break;
                case FIT_TYPE_int16:
                    printf("(%hd)", *(int16_t*)FIT_getValue(rec, f));
                    break;
                case FIT_TYPE_uint16:
                    printf("(%hu)", *(uint16_t*)FIT_getValue(rec, f));
                    break;
                case FIT_TYPE_int32:
                    printf("(%d)", *(int32_t*)FIT_getValue(rec, f));
                    break;
                case FIT_TYPE_uint32:
                    printf("(%u)", *(uint32_t*)FIT_getValue(rec, f));
                    break;
                case FIT_TYPE_int64:
                    printf("(%"PRId64")", *(int64_t*)FIT_getValue(rec, f));
                    break;
                case FIT_TYPE_uint64:
                    printf("(%"PRIu64")", *(uint64_t*)FIT_getValue(rec, f));
                    break;
                default:
                //case FIT_TYPE_bytes:
                    {
                        int i;
                        uint8_t * p = FIT_getValue(rec, f);
                        for (i = 0; i < ffi.size; ++i) {
                            printf("%02x ", p[i]);
                        }
                    }
                    break;
                //default:
                //    printf("Type: '%d 0x%02x'", ffi.type, ffi.type);
                //    break;
             }

            puts("");

        }


    }

    FIT_close(f);
}



int main(int argc, char *argv[])
{
    for (int i = 1; i < argc; ++i)
        dump_file(argv[i]);

    return 0;
}
