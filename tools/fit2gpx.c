#include <getopt.h>
#include <libgen.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>

#include "libfit.h"
#include "libfit-messages.h"

#define die(...) do { fprintf(stderr, __VA_ARGS__); exit(1); } while (0)

#define DEFAULT_OUTFILE "out.gpx"

struct params
{
    char *out;
    char *fit_file;
};

void usage(char *name)
{
    printf("usage: %s [--out gpx_file] FIT\n", basename(name));
    printf("       %s [--help]\n", basename(name));
    printf("       %s [--version]\n", basename(name));
    printf("  -o, --out gpx_file: file to generate, default %s\n",
           DEFAULT_OUTFILE);
}

void parse(int argc, char *argv[], struct params *par)
{
    while (1)
    {
        static struct option long_options[] =
        {
            {"out", required_argument, 0, 'o'},
            {"help", no_argument, 0, 'h'},
            {"version", no_argument, 0, 'v'},
            {0, 0, 0, 0}
        };

        int c = getopt_long(argc, argv, "o:hv", long_options, NULL);
        if (c == -1)
        {
            if (!argv[optind])
            {
                die("no file to process\n");
            }
            else
            {
                /* assert optind is the last parameter */
                if (optind != (argc-1))
                {
                    die("Only one file to process");
                }
                par->fit_file = strdup(argv[optind]);
            }
            break;
        }
        switch (c)
        {
        case 'o':
            par->out = strdup(optarg);
            break;

        case 'v':
            puts(FIT_info());
            break;

        case 'h':
            usage(*argv);
            exit(0);
            break;

        default:
            die("bad param: %s\n", optarg);
        }
    }
    if (!par->out)
    {
        par->out = strdup(DEFAULT_OUTFILE);
    }
}

struct point
{
    /* position, read in degree then change to pixel  */
    double pos[2];
    /* elevation */
    double ele;
    /* speed in km/h */
    uint32_t ts;
    struct point *next;
};


struct point *extract_data(const char *file, struct point *points, char *timestamp)
{
    /* openfile */
    struct point *ret = points, *last = points;

    fit_file_t *f;
    fit_record_t *rec = NULL;
    f = FIT_open(file);

    uint32_t start_timestamp = 0;

    if (NULL == f) 
    {
        die("cannot dump %s\n", file);
    }

    while (1) 
    {
        /* read next record */
        fit_record_t *rc = FIT_readRecord(f, rec);
        if (!rc) 
        {
            FIT_free(rec);
            break;
        }
        rec = rc;

        /* the first record: read timestamp*/
        if (FIT_GLOBAL_FILE_ID == FIT_getRecordType(rec))
        {
            if (1 == FIT_getValues(rec, FIT_GLOBAL_FILE_ID, FIT_FILE_TIME_STAMP, &start_timestamp, -1))
            {
                if (timestamp) 
                {
                    time_t ts = start_timestamp;
                    struct tm res;
                    localtime_r(&ts, &res);
                    /* @TODO correct this (not important: only send to stderr today)*/
                    sprintf(timestamp, "%4d-%02d-%02dT%02d:%02d:%02d",
                            res.tm_year+1920, res.tm_mon+1, res.tm_mday-1,
                            res.tm_hour, res.tm_min, res.tm_sec);
                }
            }
            continue;
        }

        /* if record is not a record, skip it */
        if (FIT_GLOBAL_RECORD != FIT_getRecordType(rec))
        {
            continue;
        }

        double x=NAN, y=NAN, e=NAN;
        int32_t ts = 0;
        int good;
        /* get timestamp */
        FIT_getValues(rec, FIT_GLOBAL_RECORD, FIT_RECORD_TIMESTAMP_U32, &ts, -1);

        /* decode position and speed */
        good =  FIT_decodeValues(rec, FIT_GLOBAL_RECORD,
                FIT_RECORD_POSITION_LONG_S32, &x,
                FIT_RECORD_POSITION_LAT_S32, &y,
                FIT_RECORD_ALTITUDE_U16, &e, -1);

        if (good >= 2)
        {
            struct point *pt = calloc(1, sizeof *pt);
            if (NULL == pt)
            {
                die("calloc failed");
            }

            pt->pos[0] = x;
            pt->pos[1] = y;
            pt->ele = e;
            pt->ts = ts;
            if (NULL == ret)
            {
                ret = pt;
            }
            else
            {
                last->next = pt;
            }

            last = pt;
        }
    }
    FIT_close(f);
    return ret;
}

void free_data(struct point *pts)
{
    struct point *next;
    while (pts)
    {
        next = pts->next;
        free(pts);
        pts = next;
    }
}

#define CAST (const xmlChar*)

void generate_xml(const char *filename, struct point *pts)
{
    xmlTextWriterPtr writer;
    writer = xmlNewTextWriterFilename(filename, 0);

    if (NULL == writer)
    {
        die("Can't create %s\n", filename);
    }

    xmlTextWriterStartDocument(writer, "1.0", "UTF-8", NULL);

    xmlTextWriterStartElement(writer, CAST "gpx");

    xmlTextWriterWriteAttribute(writer, CAST "xmlns", \
          CAST "http://www.topografix.com/GPX/1/1");
    xmlTextWriterWriteAttribute(writer, CAST "version", CAST "1.1" );
    xmlTextWriterWriteAttribute(writer, CAST "xmlns:xsi", \
          CAST "http://www.w3.org/2001/XMLSchema-instance");
    xmlTextWriterWriteAttribute(writer, CAST "xsi:schemaLocation", \
          CAST "http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd");

    xmlTextWriterStartElement(writer, CAST "trk");

    xmlTextWriterWriteElement(writer, CAST "name", CAST filename);

    xmlTextWriterStartElement(writer, CAST "trkseg");

    while (pts)
    {
        char buffer[32];
        if (isnan(pts->pos[0]) || isnan(pts->pos[1]))
        {
            pts = pts->next;
            continue;
        }

        xmlTextWriterStartElement(writer, CAST "trkpt");

        sprintf(buffer, "%g", pts->pos[1]);
        xmlTextWriterWriteAttribute(writer, CAST "lat", CAST buffer);
        sprintf(buffer, "%g", pts->pos[0]);
        xmlTextWriterWriteAttribute(writer, CAST "lon", CAST buffer);

        if (!isnan(pts->ele))
        {
            sprintf(buffer, "%g", pts->ele);
            xmlTextWriterWriteElement(writer, CAST "ele", CAST buffer);
        }

        struct tm * tm = gmtime((const time_t*) & pts->ts);
        tm->tm_year += 20;
        tm->tm_mday -= 1;
        strftime(buffer, sizeof buffer, "%FT%T", tm);
        xmlTextWriterWriteElement(writer, CAST "time", CAST buffer);

        // close trkpt
        xmlTextWriterEndElement(writer);
        pts = pts->next;
    }


    // close trkseg
    xmlTextWriterEndElement(writer);

    // close trk
    xmlTextWriterEndElement(writer);

    // end gpx
    xmlTextWriterEndElement(writer);

    // end file
    xmlTextWriterEndDocument(writer);

    xmlFreeTextWriter(writer);
}

int main(int argc, char *argv[])
{
    struct params params = { NULL, NULL };

    parse(argc, argv, &params);
    struct point *pts;
    char timestamp[20];
    pts = extract_data(params.fit_file, NULL, timestamp);

    /* coordinates */
    generate_xml(params.out, pts);

    /* clean up */
    free_data(pts);
    free(params.fit_file);
    free(params.out);

    return 0;
}
