var map;
var paths = {};
var heights = {};
var speeds = {};
var eles = {};
var hrs = {};

// list containing the sum of run time (in seconds)
// for given week, month, year
// for instance times["m2018-10"] = 1523, times["all"] = 1548323
var times = {};
// number of runs for given week, month, year
// for instance runs["m2018-10"] = 2, runs["all"] = 486
var runs = {};
// list containing runned distance in m for given week, month, year
// for instance distances["m2018-10"] = 1523, distances["all"] = 1548323
var distances = {};
// list of array containing runs indexes for given week, month, year
// for instance indexes["w2017-15"] = [15, 16, 18]
var indexes = {};

var graphs = {};
var plots = ["speed", "elevation"]

// weeks: array containing the known runned weeks.
// for instance weeks[5] = "w2018-05"
var weeks = [];
// months: array containing the known runned months.
// for instance months[5] = "m2018-05"
var months = [];
// years: array containing the know runned years.
// for instance years[5] = "y2018"
var years = [];

let data;

function init(base) {
    var szAttr = '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors<\/a>';
    // add the OpenStreetMap tiles
    var layer_osm = L.tileLayer(
        'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: szAttr
    });

    var layer_landscape =  L.tileLayer(
        'https://tile.thunderforest.com/landscape/{z}/{x}/{y}.png@APIKEY@', {
        maxZoom: 19,
        attribution: szAttr
    });

    var layer_outdoors =  L.tileLayer(
        'https://tile.thunderforest.com/outdoors/{z}/{x}/{y}.png@APIKEY@', {
        maxZoom: 19,
        attribution: szAttr
    });

    map = L.map('map', {
        center: [0, 0],
        zoom: 13,
        layers: [layer_outdoors]
    });


    // show the scale bar on the lower left corner
    L.control.scale().addTo(map);

    var baseMaps = {
        "osm": layer_osm,
        "landscape": layer_landscape,
        "outdoors": layer_outdoors
    };

    var overlayMaps = {};

    L.control.layers(baseMaps).addTo(map);

    if (base) {
        load_runs(base+'.json');
    } else {
        load_runs('base.json');
    }
}

async function get_data(url) {
    const ans = await fetch(url);
    return ans.json()
}

function get_distance(data) {
    var text;
    var km;

    km = data.distance[data.distance.length-1] / 1000.;

    text = km.toFixed(2) + " km" ;

    return text;
}

function getTime(data) {
    var text = "";
    var h, m, s;
    s = parseInt(data.time[data.time.length-1]);
    h = parseInt(s/3600);
    s %= 3600;
    m = parseInt(s/60);
    s %= 60;

    if (h > 0)
        text = h + " h ";
    text += m + " m " + s + " s";
    return text;
}

function get_pace(data) {
    var text;
    var time = data.time[data.time.length-1] / 60.;
    var dist = data.distance[data.distance.length-1]/ 1000. ;
    var pace = time / dist;

    //@todo Format the pace in right form: min:sec par km
}

function get_speed(data) {
    var text;
    var time = data.time[data.time.length-1] / 3600.;
    var dist = data.distance[data.distance.length-1]/ 1000. ;
    var speed = dist / time;
    text = speed.toFixed(1) + " km/h";
    return text;
}

function compute_stat(data, id) {
    var text = "";
    var km, m;

    text += "name: " + id + "<br/>";
    text += "time: " + getTime(data) + "<br/>";
    text += "distance: " + get_distance(data) + "<br/>";
    text += "speed: " + get_speed(data);

    return text;
}

function display_stats(data, id) {
    var stats = document.getElementById("stats");

    stats.innerHTML = compute_stat(data, id);
}

/*
function hideCurve(id) {
    var graph = graphs[id];

    if (graph) {
        graph.destroy();
    }
    graphs[id] = null;
}

*/

function filter_data(x, y) {
    var xx = [];
    var yy = [];

    for(var t = 60; ; t += 60) {
        var from = t-60;
        var to = t+60;
        var temp = [];
        for(var i = 0; i < x.length; ++i) {
            if (x[i] > to) {
                break;
            }
            if (x[i] > from) {
                temp.push(y[i]);
            }
        }
        if (0 == temp.length) {
            break;
        }
        temp.sort();
        xx.push(t);
        yy.push(temp[Math.trunc(temp.length/2)]);
    }
    return [xx, yy];
}

function extract_data(data){
	// in x (time) make round measures (like only / 15 seconds)
    var x = [];
    var y = [];

    for (i = 1; i < data.length; ++i)
    {
        x.push(data[i][0]);
        y.push(data[i][1]);

    }
    return filter_data(x, y);
}
function display_curve(data, what){

    // TODO hide curve if no data is available

    var id = what + '_plot'
    var canvas = document.getElementById(id);
    var ctx = canvas.getContext('2d');
    var graph = graphs[what];
    if (graph) {
        graph.clear();
        graph.destroy();
    }
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    xydata = extract_data(data)
    var x = xydata[0]
    var y = xydata[1]
    graph = new Chart(ctx, {
        type: 'line',
        data: {
            labels: x,
            datasets:[{
                data: y,
                fill: false,
                label: what
            }]},
        options :{
            scales :{
                x: {
                    ticks: {
                        // Include a dollar sign in the ticks
                        callback: function(value, index, ticks) {
                            console.log("Hello " + value);
                            var m, h;
                            if (value > 60) {
                                h = Math.round(value / 60);
                                m = value % 60;
                                return h + ':' + m;
                            } else {
                                return  value;
                            }
                        }
                    }
                }
            }
        }

    });
    graphs[what] = graph;


}

/*
function displayCurve(x, y, id) {
    var canvas = document.getElementById(plots[id]+"plot");
    var ctx = canvas.getContext('2d');
    var graph = graphs[id];

    if (graph) {
        graph.clear();
        graph.destroy();
    }
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    graph = new Chart(ctx, {
        type: 'line',
        data: {
            labels: x,
            datasets:[{
                data: y
            }]
        }
    });
    graphs[id] = graph;
}

*/
/*
function build_data_set(y, x){
    var data = [];

    for (i = 1; i < x.length; ++i)
    {
        var pt = { "x": parseFloat(x[i]), "y": parseFloat(y[i])};
        data.push(pt);
    }
    return data;
}
*/

async function click_run(ID) {

    // TODO make the selected run bold, clear the other
    console.log("click_run('" + ID + "')");

    if (!paths[ID]) {
        // if path has not been loaded yet, load it
        var url = "maps/" + ID;
        var data = await get_data(url);

        paths[ID] = {
            path: L.geoJSON(data.path),
            pos: data.pos.coordinates,
            name: ID,
            stats: compute_stat(data, ID),
            displayed: false
        };
        heights[ID] = data.height;
        speeds[ID] = data.speed;
        distances[ID] = data.distance;
		// TODO add some try catch here...
		eles[ID] = data.altitude;
		hrs[ID] = data.heart_rate;
    }

    for (id in paths) {
        // hide the path
        paths[id].path.removeFrom(map);
    }

    // show the path
    map.setView({
        lon: paths[ID].pos[0],
        lat: paths[ID].pos[1]});
    paths[ID].path.addTo(map);

    // display speed (km/h = f(t))
    display_curve(speeds[ID], "speed");
    display_curve(eles[ID], "ele");
    display_curve(hrs[ID], "hr");
	// altitude ele_plot
	// heart_rate hr_plot
}

function distance_to_str(distance_m) {
    return (distance_m/1000).toFixed(1) + " km";
}

function speed_to_str(speed_m_p_sec) {
    return (speed_m_p_sec*3.6).toFixed(1) + " km/h";
}

function time_to_str(time_s) {
    console.log("time_to_str(" + time_s + ")");
    let ret = "";
    let days, hour, min, tot;
    tot =  parseInt(time_s/60);
        min =  parseInt(tot % 60);
        tot =  parseInt(tot/60);
        hour = parseInt(tot % 24);
        day =  parseInt(tot / 24);

        if (min < 10) {
            min = "0" + min
        }
        if (hour < 10) {
            hour = "0" + hour
        }

        switch (day) {
            case 0:
                break;
            case 1:
            ret = "1 day ";
                break
            default:
            ret = day + " days ";
        }
    ret += hour + ":" + min;
    return ret;
}

function display_info(prefix, runs, distance, time) {
    console.log("want + '" + prefix + "run'")
    document.getElementById(prefix + "run").innerHTML = runs + " Runs"
    if (runs > 0) {
        document.getElementById(prefix + "distance").innerHTML = distance_to_str (distance) ;
        document.getElementById(prefix + "distanceperrun").innerHTML = (distance/1000/runs).toFixed(2) + " km/run";
        document.getElementById(prefix + "speed").innerHTML = speed_to_str(distance/time);
        document.getElementById(prefix + "time").innerHTML = time_to_str(time);
    } else {
        document.getElementById(prefix + "distance").innerHTML = "" ;
        document.getElementById(prefix + "distanceperrun").innerHTML = "" ;
        document.getElementById(prefix + "speed").innerHTML = "" ;
        document.getElementById(prefix + "time").innerHTML = "" ;

    }
}

function display_global_info(runs, distance, time) {
    display_info("global", runs, distance, time);
}

function un_select() {
    document.getElementById("week").style.fontWeight = "normal" ;
    document.getElementById("month").style.fontWeight = "normal" ;
    document.getElementById("year").style.fontWeight = "normal" ;
    document.getElementById("all").style.fontWeight = "normal" ;
}

function get_run(idx) {
    let ret = '';

    let id = data[idx]["ID"];
    let date = data[idx]["date"];
    let distance = data[idx]["total_distance"];
    let time = data[idx]["total_time"];
    ret =  "<div class=\"run\" onclick=\"click_run('"+ id +"')\">" ;
    ret += "<span class=\"runlarge\">";
    ret +=  date + "&nbsp;" ;
    ret += "</span>";
    ret += "<span class=\"runthin\">";
    ret +=  distance_to_str( distance ) + "&nbsp;" ;
    ret += "</span>";
    ret += "<span class=\"runthin\">";
    ret +=  time_to_str ( time ) + "&nbsp;" ;
    ret += "</span>";
    ret += "<span class=\"runlarge\">";
    ret +=  speed_to_str(distance / time ) + "&nbsp;" ;
    ret += "</span>";
    ret += "</div>";

    return ret;
}


// display a week, month, year or all
// what: the thing to display, can be "w2019-04" for a week
// whats: the arrays of things: weeks, months...
function on_what(what, whats, what_str) {
    un_select();
    document.getElementById(what_str).style.fontWeight = "bold" ;
    if ('' == what) {
        // if what is not set, get the more recent data
        what = whats[whats.length - 1];
        console.log("what is now '" + what + "'");
    }

    // find next and prev week
    let idx = whats.indexOf(what);
    let prev = idx - 1;
    let sprev = ""

    if (prev != -1) {
        sprev = "<span onclick=\"on_" + what_str +  "('" + whats[prev] + "')\">" + (whats[prev]).slice(1)+"</span>";
    }
    let next = idx + 1;
    let snext = "";
    if (next != whats.length) {
        snext = "<span onclick=\"on_" + what_str + "('" + whats[next] + "')\">" + (whats[next]).slice(1)+"</span>";
    }

    // display week selector
    document.getElementById("timeselected").innerHTML =
        sprev + "<b>&nbsp;&nbsp;" + what.slice(1) + "&nbsp;&nbsp;</b>" + snext;

    // display week stats
    display_info("", runs[what], distances[what], times[what]);

    // display runs for that week
    let local_runs = indexes[what]

    // TODO is that useful?
    local_runs.sort() 
    let d
    let html = '<br/>'
    for (d in local_runs) {
        html += get_run(local_runs[d]);
    }
    html += '<br/>'

    document.getElementById("preview").innerHTML = html;

}

function on_week(week) {
    console.log("on_week(w" + week + ")")
    on_what(week, weeks, "week");
    }

function on_month(month) {
    console.log("on_month(m" + month + ")")
    on_what(month, months, "month");
}

function on_year(year) {
    on_what(year, years, "year");
}

function on_all() {
    console.log("on_all()")
    console.log("data.length: " + data.length)
    on_what(month, months, "month");
    un_select();
    document.getElementById("all").style.fontWeight = "bold" ;


    // display week selector
    document.getElementById("timeselected").innerHTML =
        "<b>&nbsp;&nbsp;All&nbsp;&nbsp;</b>";

    display_info("", runs["all"], distances["all"], times["all"]);

    // TODO Sort data

    // display runs 
    let html = '<br/>'
    for (let d = 0; d < data.length; ++ d) {
        html += get_run(d);
    }
    html += '<br/>'

    console.log("data added (?)")

    document.getElementById("preview").innerHTML = html;


}


Date.prototype.iso8601Week = function () {
    // Create a copy of the current date, we don't want to mutate the original
    const date = new Date(this.getTime());

    // Find Thursday of this week starting on Monday
    date.setDate(date.getDate() + 4 - (date.getDay() || 7));
    const thursday = date.getTime();

    // Find January 1st
    date.setMonth(0); // January
    date.setDate(1);  // 1st
    const jan1st = date.getTime();

    // Round the amount of days to compensate for daylight saving time
    const days = Math.round((thursday - jan1st) / 86400000); // 1 day = 86400000 ms
    ret =  Math.floor(days / 7) + 1;

    if (ret < 10) {
        ret = "0" + ret;
    } else {
        ret = "" + ret;
    }

    return ret;

};

function add_to(what, distance, time) {
    if (runs.hasOwnProperty(what)) {
        runs[what] ++
        distances[what] += distance;
        times[what] += time;
    } else {
        runs[what] = 1
        distances[what] = distance;
        times[what] = time;
    }
}

function add_to_indexes(what, data) {
    if (!indexes.hasOwnProperty(what)) {
        indexes[what] = []
    }
    indexes[what].push(String(data))
}




// load runs from  base.json
async function load_runs(url) {
    // open base.json
    data = await get_data(url);

    var week;
    var d, idx;

    runs["all"] = 0;
    times["all"] = 0;
    distances["all"] = 0;

    /* sort data by dates (old first)*/
    data.sort(function (a,b) { 
        return new Date(b["date"]) - new Date(a["date"])
    })
    for (d in data) {
        const date = new Date(data[d]["date"]);
        let y = date.getFullYear();
        let w = date.iso8601Week();
        let m = date.getMonth() + 1 ;
        if (isNaN(y) || isNaN(w) || isNaN(m)) {
            continue;
        }

        if (m < 10) {
            m = "0" + m;
        }
        week = String("w" + y + "-" + w);
        month = String("m" + y + "-" + m);
        year = String("y" + y);

        runs["all"] ++;
        distances["all"] += Number(data[d]["total_distance"]);
        times["all"] += Number(data[d]["total_time"]);
        add_to(week, Number(data[d]["total_distance"]), Number(data[d]["total_time"]));
        add_to(month, Number(data[d]["total_distance"]), Number(data[d]["total_time"]));
        add_to(year, Number(data[d]["total_distance"]), Number(data[d]["total_time"]));

        add_to_indexes(week, d);
        add_to_indexes(month, d);
        add_to_indexes(year, d);

        if (-1 ==  weeks.indexOf(week)) {
            weeks.push(week);
        }
        if (-1 ==  months.indexOf(month)) {
            months.push(month);
        }
        if (-1 ==  years.indexOf(year)) {
            years.push(year);
        }
    }
    weeks.sort();
    months.sort();
    years.sort();


    display_global_info(runs["all"], distances["all"], times["all"]);

    on_week('');
}

