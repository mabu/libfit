/**/
#include <stdio.h>
#include <limits.h>
#include "libfit.h"
#include "libfit-messages.h"

int main(int argc, char *argv[])
{
    double lat[] = {INT_MAX, INT_MIN};
    double lon[] = {INT_MAX, INT_MIN};
    while (--argc)
    {
        int cnt = 0;
        fit_record_t *rec = NULL;
        fit_file_t * f;
        f = FIT_open(argv[argc]);
        if (!f) {
            printf("%s failed\n", argv[argc]);
            break;
        }
        while (1)
        {   
            int ret; 
            double pos[2];
            ++cnt;
            fit_record_t *rc;
            rc = FIT_readRecord(f, rec);
            if (NULL == rc)
            {
                break;
            }
            rec = rc;
            //FIT_getPosition(rec, NULL, NULL);
            ret = FIT_decodeValues(rec, FIT_GLOBAL_RECORD,
                    FIT_RECORD_POSITION_LAT_S32, pos, 
                    FIT_RECORD_POSITION_LONG_S32, pos +1,
                    -1);
            if (2 == ret)
            {
                if (pos[0] < lat[0])
                {
                    lat[0] = pos[0];
                }
                else if (pos[0] > lat[1])
                {
                    lat[1] = pos[0];
                }
                if (pos[1] < lon[0])
                {
                    lon[0] = pos[1];
                }
                else if (pos[1] > lon[1])
                {
                    lon[1] = pos[1];
                }
            }
        } /* while 1*/
        if (f) { 
            printf("%s\n", argv[argc]);
            printf("lat:\t%f\t%f\n", lat[0], lat[1]);
            printf("lon:\t%f\t%f\n", lon[0], lon[1]);
        }
        FIT_close(f);
        FIT_free(rec);

    } /* while argc */


    return 0;
}
