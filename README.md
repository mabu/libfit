Building
--------

Download sources and build with: 

    git clone http://framagit.org/mabu/libfit
    cd libfit
    autoreconf --install
    ./configure
    make
    make install
