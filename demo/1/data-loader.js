// deux variable globales, pour contenir la carte et le chemin qu'on trace
var map;
var path ;

// fonction appelée au démarrage
function init() {

    // la petite ligne de copyright
    var szAttr = '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors<\/a>';

    // On choisi les tuiles osm par défaut
    var layer_osm = L.tileLayer(
        'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: szAttr
    });


    // Création de la carte avec les attributs qui vont bien:
    // * ou on regarde
    // * zoom
    // * tuiles à utiliser
    map = L.map('map', {
        center: [-49.35, 70.3],
        zoom: 13,
        layers: [layer_osm]
    });

    // Création du chemin proprement dit
    path = L.geoJSON([{
                "type": "LineString", 
                "coordinates": [[70.265, -49.354], [70.25,-49.354], [70.25,-49.35], [70.265, -49.35], [70.265, -49.354]]
    }]);
   
    // Et on affiche le chemin sur la carte
    path.addTo(map);
}
